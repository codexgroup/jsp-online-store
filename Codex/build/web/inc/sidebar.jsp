<%-- 
    Document   : sidebar
    Created on : Jan 25, 2014, 9:34:08 PM
    Author     : hikmet
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

            <div id="xx" class="sidebar" style="z-index:100">
                  <!-- Logo starts -->
                  <div class="logo">
                      <a href="index.jsp"><img style="width:100%" src="/img/logo.png"></img></a></h1>
                  </div>
                  <!-- Logo ends -->
                  <!-- Sidebar buttons starts -->
                  <div class="sidebar-buttons text-center" style="padding-bottom: 10px;">
                     <!-- User button -->
                     <%if(session.getAttribute("user")!=null){%>
                     <div class="btn-group">
                       <a href="profile.jsp" class="btn btn-black btn-xs"><i class="icon-user"></i></a>
                       <a href="profile.jsp" class="btn btn-danger btn-xs">User</a>
                     </div>
                     <!-- Lock button -->
                     <div class="btn-group">
                       <a href="lock.jsp" class="btn btn-black btn-xs"><i class="icon-lock"></i></a>
                       <a href="lock.jsp" class="btn btn-danger btn-xs">Lock</a>
                     </div>
                      <div class="btn-group">
                       <a href="login.jsp" class="btn btn-black btn-xs"><i class="icon-off"></i></a>
                       <a href="login.jsp" class="btn btn-danger btn-xs">Logout </a>
                     </div>
                     <%}else{%>
                       <div class="btn-group">
                       <a href="login.jsp" class="btn btn-black btn-xs"><i class="icon-user"></i></a>
                       <a href="login.jsp" class="btn btn-danger btn-xs">Register</a>
                     </div>
                     <!-- Logout button -->
                     <div class="btn-group">
                       <a href="login.jsp" class="btn btn-black btn-xs"><i class="icon-off"></i></a>
                       <a href="login.jsp" class="btn btn-danger btn-xs">Login </a>
                     </div><%}%>>
                  </div>
                  <!-- Sidebar buttons ends -->
                  
                  <!-- Sidebar navigation starts -->
				  
				  <div class="sidebar-dropdown"><a href="#">Navigation</a></div>
				  
                  <div class="sidey">
                     <ul class="nav">
                         <!-- Main navigation. Refer Notes.txt files for reference. -->
                         
                         <!-- Use the class "current" in main menu to hightlight current main menu -->
                         <li class="current"><a href="index.jsp"><i class="icon-home"></i> Home</a></li>
                         						 
                         <li class="has_submenu">
                             <a href="#">
                                 <i class="icon-file"></i> Categories <span class="label label-darky">${applicationScope.category.count}</span>
                                 <!-- Icon to show dropdown -->
                                 <span class="caret pull-right"></span>
                             </a>
                             <!-- Sub navigation -->
                             <ul>
                                <!-- Use the class "active" in sub menu to hightlight current sub menu -->
                                <%-- TODO for loop for categories      ${applicationScope.category.name[i]}--%>
                                <li><a href="category.jsp?id=1"><i class="icon-double-angle-right"></i>Restaurants</a></li>
                                <li><a href="category.jsp?id=2"><i class="icon-double-angle-right"></i>Fashion</a></li>
                             </ul>
                         </li>
                         <li><a href="aboutus.jsp"><i class="icon-user"></i> About Us</a></li>
						 <li><a href="contactus.jsp"><i class="icon-phone"></i> Contact Us</a></li>
                         <li class="has_submenu">
                           <a href="#">
                              <i class="icon-bug"></i> Extras <span class="label label-darky">3</span>
                              <span class="caret pull-right"></span>
                           </a>
                             <ul>
                                 <li><a href="service.jsp"><i class="icon-double-angle-right"></i> Services</a></li>
                                 <li><a href="terms.jsp"><i class="icon-double-angle-right"></i> Terms and conditions</a></li>
                                 <li><a href="faq.jsp"><i class="icon-double-angle-right"></i> FAQ</a></li>
                             </ul>
                         </li>
                                                  
                     </ul>               
                  </div>
                  <!-- Sidebar navigation ends -->
                  <!-- Sidebar status ends -->
            </div>