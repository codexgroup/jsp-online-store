<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<!-- Title here -->
		<title>Profile</title>
		<!-- Description, Keywords and Author -->
		<meta name="description" content="Your description">
		<meta name="keywords" content="Your,Keywords">
		<meta name="author" content="ResponsiveWebInc">
      
		<!-- Google web fonts -->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<!-- Styles -->
		<!-- Bootstrap CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<!-- jQuery UI -->
		<link href="css/jquery-ui.css" rel="stylesheet">
		
		
		<!-- jQuery Gritter -->
		<link href="css/jquery.gritter.css" rel="stylesheet">
		<!-- Bootstrap Switch -->
		<link href="css/bootstrap-switch.css" rel="stylesheet">
		<!-- jQuery Datatables -->
		<link href="css/jquery.dataTables.css" rel="stylesheet">
		<!-- Rateit -->
		<link href="css/rateit.css" rel="stylesheet">
		<!-- jQuery prettyPhoto -->
		<link href="css/prettyPhoto.css" rel="stylesheet">
		<!-- Full calendar -->
		<link href="css/fullcalendar.css" rel="stylesheet">		
		<!-- Font awesome CSS -->
		<link href="css/font-awesome.min.css" rel="stylesheet">		
		<!-- Custom CSS -->
		<link href="css/adminstyle.css" rel="stylesheet">
		
		<!--[if IE]>
			<link rel="stylesheet" type="text/css" href="css/adminstyle-ie.css" />
		<![endif]-->
            
		<!-- Favicon -->
		<link rel="shortcut icon" href="#">
	</head>
	
	<body>

      <div class="out-container">
         <div class="outer">
            <!-- Sidebar starts -->
            <%@include file="/inc/sidebar.jsp" %>
            <!-- Sidebar ends -->
            
            <!-- Mainbar starts -->
            <div class="mainbar">
				<!-- Black block starts -->
				<div class="blue-block">
					<div class="page-title">
						<h3 class="pull-left"><i class="icon-desktop"></i> Profile </h3> 	
						<div class="breads pull-right">
							 <!-- Sidebar buttons starts -->
                 
                  <!-- Sidebar buttons ends -->
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<!-- Black block ends -->
				
				
				
				<!-- Content starts -->
				
				<div class="container">
					<div class="page-content page-profile">
						
						<div class="page-tabs">
						
							<!-- Nav tabs -->
							<ul class="nav nav-tabs">
							  <li class="active"><a href="#profile" data-toggle="tab">Profile</a></li>
							  <li><a href="#update" data-toggle="tab">Update Profile</a></li>
							  <li><a href="#activity" data-toggle="tab">Recent Activity</a></li>
							</ul>

							<!-- Tab panes -->
							<div class="tab-content">
							
							  <!-- Profile tab -->
							  <div class="tab-pane fade active in" id="profile">
								<h4>Your Profile</h4>
								
								<div class="row">
                                 <div class="col-md-3 col-sm-3 text-center">
									<!-- Profile pic -->
                                    <a href="#"><img src="img/user-big.jpg" class="img-thumbnail img-circle img-responsive" alt="" /></a>
                                 </div>
                                 <div class="col-md-9 col-sm-9">
									<!-- Profile details -->
                                    <table class="table table-bordered">
                                    
                                       <tr>
                                          <td class="active"><strong>Name</strong></td>
                                          <td>${sessionScope.user.name}&nbsp;${sessionScope.user.surname}</td>
                                       </tr>
									<%--   <tr>
										  <td class="active"><strong>Points</strong></td>
										  <td>
											<span class="label label-danger">50.00</span>
											<span class="label label-success">50.00</span>
											<span class="label label-warning">50.00</span>
										  </td>
									   </tr>--%>
                                       <tr>
                                          <td class="active"><strong>Email</strong></td>
                                          <td>${sessionScope.user.email}</td>
                                       </tr>
                                       <tr>
                                          <td class="active"><strong>Phone Number</strong></td>
                                          <td>${sessionScope.user.phoneNumber}</td>
                                       </tr>
                                       <tr>
                                          <td class="active"><strong>Address</strong></td>
                                          <td>${sessionScope.user.address}</td>
                                       </tr>
                                       <tr>
                                          <td class="active"><strong>Date Joined</strong></td>
                                          <td>${sessionScope.user.registerDate}</td>
                                       </tr>
                                       <%--
									   <tr>
										  <td class="active"><strong>Last Login</strong></td>
										  <td>26.12.2013</td>
									   </tr>
									   <tr>
										  <td class="active"><strong>IP Address</strong></td>
										  <td><span class="label label-danger">583.850.291.1990</span></td>
									   </tr>--%>
                                    </table>
                                 </div>
                              </div>
								
							  </div>
							  
							  <!-- Update profile tab -->
							  <div class="tab-pane fade" id="update">
								<h4>Update Profile</h4>
								
									<form class="form-horizontal">
                                          <!-- Name -->
                                          <div class="form-group">
                                            <label class="control-label col-lg-2" for="name1">First name</label>
                                            <div class="col-lg-6">
                                              <input type="text" name="name" value="${sessionScope.user.name}" class="form-control" id="name1">
                                            </div>
                                          </div>   <!-- SurName -->
                                          <div class="form-group">
                                            <label class="control-label col-lg-2" for="name1">Surname</label>
                                            <div class="col-lg-6">
                                              <input  type="text" name = "surname" value="${sessionScope.user.surname}" class="form-control" id="surname1">
                                            </div>
                                          </div>   
                                          <!-- Email -->
                                          <div class="form-group">
                                            <label class="control-label col-lg-2" for="email1">Email</label>
                                            <div class="col-lg-6">
                                              <input type="text" name = "email" value="${sessionScope.user.email}" class="form-control" id="email1">
                                            </div>
                                          </div>
                                          <!-- Telephone -->
                                          <div class="form-group">
                                            <label class="control-label col-lg-2" for="phoneNumber">Phone Number</label>
                                            <div class="col-lg-6">
                                              <input type="text" name = "phoneNumber" value="${sessionScope.user.phoneNumber}" class="form-control" id="phoneNumber">
                                            </div>
                                          </div>  
                                          <!-- Address -->
                                          <div class="form-group">
                                            <label class="control-label col-lg-2" for="address">Address</label>
                                            <div class="col-lg-6">
                                              <textarea name ="address" value="${sessionScope.user.address}" class="form-control" id="address"></textarea>
                                            </div>
                                          </div>                                   
                                          <!-- City -->
                                          <div class="form-group">
                                            <label class="control-label col-lg-2" for="city">City</label>
                                            <div class="col-lg-6">
                                              <input type="text" name="city" value="${sessionScope.user.city}" class="form-control" id="city">
                                            </div>
                                          </div>        
                                    
                                          <!-- Password -->
                                          <div  class="form-group">
                                            <label class="control-label col-lg-2" for="newPassword">New Password</label>
                                            <div class="col-lg-6">
                                              <input type="password" name="newPassword"  class="form-control" id="newPassword">
                                            </div>
                                          </div>  
                                          <!-- Password -->
                                          <div  class="form-group" id="confirmNewPasswordDiv" >
                                            <label class="control-label col-lg-2" for="confirmNewPassword">Confirm New Password</label>
                                            <div class="col-lg-6">
                                              <input type="password" name="confirmNewPassword" class="form-control" id="confirmNewPassword">
                                            </div>
                                          </div>  
                                          <!-- Password -->
                                          <div  class="form-group" >
                                            <label class="control-label col-lg-2" for="password2">Current Password</label>
                                            <div class="col-lg-6">
                                              <input type="password" class="form-control" id="password2">
                                            </div>
                                          </div>
                                          <!-- Checkbox -->
                                         <%-- <div class="form-group">
                                             <div class="col-lg-6 col-lg-offset-2">
												
                                                <label class="checkbox inline">
                                                   <input type="checkbox" name ="terms" id="inlineCheckbox3" value="agree"> Agree with Terms and Conditions
                                                </label>
                                             </div>
                                          </div> 
                                          --%>
                                          <!-- Buttons -->
                                          <div class="form-group">
                                             <!-- Buttons -->
                                              <div class="col-lg-6 col-lg-offset-2">
                                                <button type="submit" class="btn btn-info">Update</button>
                                                <button type="reset" class="btn btn-default">Reset</button>
                                             </div>
                                          </div>
                                      </form>
									
							  </div>
							  
							  <!-- Activity tab -->
							  <div class="tab-pane fade" id="activity">
								<h4>Favorites</h4>

								<!-- Recent activity table -->
								<div class="table-responsive">
									<table class="table table-bordered">
										<tr>
											<th>Priority</th>
											<th>Category</th>
											<th>Favorite Place</th>
										</tr>
										<tr>
											<td>1</td>
											<td><span class="label label-danger">01/01/2013</span></td>
											<td>Lorem ipsum dolor sit amet, consectetur augue ut venenatis hendrerit, neque mauris convallis justo.</td>
										</tr>
									</table>
								</div>
								
							  </div>
							  
							</div>
						
						</div>
						
					</div>
				</div>
				
				<!-- Content ends -->		
                                
			   
            </div>
            <!-- Mainbar ends -->
            
            <div class="clearfix"></div>
         </div>
      </div>
		
      <!-- Sliding boxses starts -->
      
              
         <!-- Status slide box starts -->
         
         <!-- Status slide box ends -->    

		 <!-- Notification slide box starts -->
		 <!-- Notification slide box ends -->
         
         <!-- ////////////////////////////////// -->
      
      <!-- Sliding boxes ends -->
      
      <!-- Scroll to top -->
      <span class="totop"><a href="#"><i class="icon-chevron-up"></i></a></span> 
		
	  <!-- Javascript files -->
	  <!-- jQuery -->
	  <script src="js/jquery.js"></script>
	  <!-- Bootstrap JS -->
	  <script src="js/bootstrap.min.js"></script>
      <!-- Sparkline for Mini charts -->
      <script src="js/sparkline.js"></script>
      <!-- jQuery UI -->
      <script src="js/jquery-ui-1.10.2.custom.min.js"></script>
      
      <!-- jQuery flot -->
      <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->
      <script src="js/jquery.flot.min.js"></script>     
      <script src="js/jquery.flot.pie.min.js"></script>
      <script src="js/jquery.flot.resize.min.js"></script>
	  
	  <!-- jQuery Knob -->
	  <script src="js/jquery.knob.js"></script>
	  <!-- jQuery Data Tables -->
	  <script src="js/jquery.dataTables.min.js"></script>
	  <!-- jQuery Knob -->
	  <script src="js/bootstrap-switch.min.js"></script>
	  <!-- jQuery Knob -->
	  <script src="js/jquery.rateit.min.js"></script>
	  <!-- jQuery prettyPhoto -->
	  <script src="js/jquery.prettyPhoto.js"></script>
	  <!-- jquery slim scroll -->
	  <script src="js/jquery.slimscroll.min.js"></script>
	  <!-- jQuery gritter -->
	  <script src="js/jquery.gritter.min.js"></script>
	  <!-- jQuery full calendar -->
	  <script src="js/fullcalendar.min.js"></script>
	  <!-- Respond JS for IE8 -->
	  <script src="js/respond.min.js"></script>
	  <!-- HTML5 Support for IE -->
	  <script src="js/html5shiv.js"></script>
	  <!-- Custom JS 
	  <script src="js/custom.notification.js"></script>-->
	  <script src="js/custom2.js"></script>
      <script src="js/mapLoad.js"></script>  
      <script type="text/javascript"
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDGtyKh29ICPz1y0w4j6jyZmHK--VUHPwA&sensor=false">
     </script>
      <!-- Javascript for this page -->
      
      <script type="text/javascript">
         
         <!-- Quick chart -->         
         $(function() {
            var d1 = [[0, 1], [1, 2], [2, 3], [3, 4],[4, 5], [5, 6], [6, 5], [7, 6],[8, 7], [9, 6], [10, 7], [11, 8],[12, 9], [13, 9], [14, 9], [15, 8],[16, 8], [17, 9], [18, 10], [19, 10],[20, 9], [21, 8], [22, 7], [23, 8],[24, 9], [25, 8], [26, 6], [27, 4],[28, 2]];
			var options = {
               series: {
                  lines: {
                     show: true, fill: true, fillColor: "rgba(113,214,232,0.5)", lineWidth:1
                  },
                  points: {
                     show: true, fill: true, fillColor: "rgba(113,214,232,0.5)"
                  }
               },
               colors :["rgba(255,255,255,1)"],
               grid: {
                  color: "#fff", borderWidth: 0, minBorderMargin: 5
               },
               xaxis: {
                  font: {
                        size: 10,
                        color: "rgba(255,255,255,0.8)"
                    }
               },
               yaxis: {
                  font: {
                        size: 10,
                        color: "rgba(255,255,255,0.8)"
                    }
               }
            };
            <!-- Actual plotting -->
            $.plot("#quick-chart", [ d1 ], options);
         });
      
	  
		<!-- ECG chart -->         
         $(function() {
            var d1 = [[0,0], [1, 2], [2, 0], [3, 3], [4, 1], [5, 3], [6, 2], [7, 1], [8, 2], [9, 3], [10, 1], [11,2], [12,0]];
			var options = {
               series: {
                  lines: {
                     show: true, fill: false, lineWidth:1
                  },
                  points: {
                     show: true, fill: false, radius:2
                  }
               },
               colors :["rgba(255,255,255,1)"],
               grid: {
                  color: "#fff", borderWidth: 0, minBorderMargin: 8
               },
               xaxis: {
				  show: false
               },
               yaxis: {
				  show: false
               }
            };
            <!-- Actual plotting -->
            $.plot("#ecg-chart", [ d1 ], options);
         });
		 
         
		 
      </script>
      
	</body>	
</html>