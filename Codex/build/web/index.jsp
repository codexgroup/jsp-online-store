<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <!-- Title here -->
        <title>Tezbazar</title>
        <!-- Description, Keywords and Author -->
        <meta name="description" content="Your description">
        <meta name="keywords" content="Your,Keywords">
        <meta name="author" content="ResponsiveWebInc">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Google web fonts -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,600' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>

        <!-- Styles -->
        <!-- Bootstrap CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- Slider Revolution CSS -->
        <link href="css/settings.css" rel="stylesheet">
        <!--[if IE 8]>
        <link rel="stylesheet" href="css/settings-ie8.css">
        <![endif]-->
        <!-- Animate CSS -->
        <link href="css/animate.min.css" rel="stylesheet">
        <!-- jQuery prettyPhoto -->
        <link href="css/prettyPhoto.css" rel="stylesheet">
        <!-- Font awesome CSS -->
        <link href="css/font-awesome.min.css" rel="stylesheet">		
        <!-- Custom CSS -->
        <link href="css/style.css" rel="stylesheet">
        <link href="css/adminstyle.css" rel="stylesheet">
        <!--[if IE]>
                <link rel="stylesheet" type="text/css" href="css/style-ie.css" />
        <![endif]-->

        <!-- Favicon -->
        <link rel="shortcut icon" href="#">
    </head>

    <body>


        <div class="out-container">
            <div class="outer">
                <!-- Sidebar starts -->
                <%@include file="/inc/sidebar.jsp" %>
                <!-- Sidebar ends -->


                <!-- Mainbar starts -->
                <style type="text/css">
                     #xxxx{
                         padding-top:26px;
                        position:absolute; 
                        z-index:200; 
                        margin-left:-10px; 
                        top:350px;
                  display: block;
                  width: 20px;
                  height: 80px;
                   color: #fff;
                   text-align: center;
                   line-height: 30px;
                  background: #c20d23 url("../img/red-back.png") repeat;
     }
                </style>
       
                <!-- Mainbar starts -->
                <div class="mainbar">
           <span id="xxxx" style="display:block; border-radius: 10px;"><i class="icon-chevron-left"></i></span> 

                    <!-- Revolution Slider Start -->
                    <!-- slider container -->
                    <div class="banner-container">
                        <!-- slider -->
                        <div class="banner">
                            <!-- slider content -->
                            <ul>
                                <!-- slider first frame -->
                                <!-- SLIDE ONE -->
                                <li data-transition="fade" data-slotamount="7" data-masterspeed="500" >
                                    <!-- MAIN IMAGE -->
                                    <img src="img/slider/1.jpg" alt="slidebg1" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
                                    <!-- Inside LAYERS // heading -->
                                    <div class="tp-caption very_large_text sft heading"
                                         data-x="center"
                                         data-y="top"
                                         data-voffset="60"
                                         data-speed="700"
                                         data-start="1200"
                                         data-easing="Power4.easeOut"
                                         data-endspeed="300"
                                         data-endeasing="Power1.easeIn" 
                                         data-captionhidden="on">Welcome to Tezbazar
                                    </div>
                                    <!-- Inside LAYERS // center image -->
                                    <div class="tp-caption lft"
                                         data-x="center"
                                         data-y="center"
                                         data-voffset="20"
                                         data-speed="500"
                                         data-start="2000"
                                         data-easing="Power4.easeOut"
                                         data-endspeed="300"
                                         data-endeasing="Power1.easeIn" 
                                         data-captionhidden="on"> <img src="img/slider/s1.png" class="img-responsive" alt="" />
                                    </div>
                                    <!-- Inside LAYERS // top-left-circle -->
                                    <div class="tp-caption sfl circle-left"
                                         data-x="center"
                                         data-hoffset="-350"
                                         data-y="center"
                                         data-voffset="-70"
                                         data-speed="1000"
                                         data-start="2500"
                                         data-easing="Power4.easeOut"
                                         data-endspeed="300"
                                         data-endeasing="Power1.easeIn" 
                                         data-captionhidden="on"> <div class="caption-item"><i class="icon-globe"></i></div>
                                    </div>
                                    <!-- Inside LAYERS // top-left-arrow -->
                                    <div class="tp-caption sfl"
                                         data-x="center"
                                         data-hoffset="-250"
                                         data-y="center"
                                         data-voffset="-120"
                                         data-speed="500"
                                         data-start="2800"
                                         data-easing="Power4.easeOut"
                                         data-endspeed="300"
                                         data-endeasing="Power1.easeIn" 
                                         data-captionhidden="on"> <img src="img/slider/arrow-top-left.png" class="img-responsive arrow" alt="" />
                                    </div>
                                    <!-- Inside LAYERS // top-right-circle -->
                                    <div class="tp-caption sfr circle-right"
                                         data-x="center"
                                         data-hoffset="350"
                                         data-y="center"
                                         data-voffset="-70"
                                         data-speed="1000"
                                         data-start="3500"
                                         data-easing="Power4.easeOut"
                                         data-endspeed="300"
                                         data-endeasing="Power1.easeIn" 
                                         data-captionhidden="on"> <div class="caption-item"><i class="icon-user"></i></div>
                                    </div>
                                    <!-- Inside LAYERS // top-right-arrow -->
                                    <div class="tp-caption sfr"
                                         data-x="center"
                                         data-hoffset="250"
                                         data-y="center"
                                         data-voffset="-120"
                                         data-speed="500"
                                         data-start="3800"
                                         data-easing="Power4.easeOut"
                                         data-endspeed="300"
                                         data-endeasing="Power1.easeIn" 
                                         data-captionhidden="on"> <img src="img/slider/arrow-top-right.png" class="img-responsive arrow" alt="" />
                                    </div>
                                    <!-- Inside LAYERS // bottom-right-circle -->
                                    <div class="tp-caption sfr circle-right"
                                         data-x="center"
                                         data-hoffset="350"
                                         data-y="center"
                                         data-voffset="120"
                                         data-speed="1000"
                                         data-start="4500"
                                         data-easing="Power4.easeOut"
                                         data-endspeed="300"
                                         data-endeasing="Power1.easeIn" 
                                         data-captionhidden="on"> <div class="caption-item"><i class="icon-suitcase"></i></div>
                                    </div>
                                    <!-- Inside LAYERS // bottom-right-arrow -->
                                    <div class="tp-caption sfr"
                                         data-x="center"
                                         data-hoffset="240"
                                         data-y="center"
                                         data-voffset="160"
                                         data-speed="500"
                                         data-start="4800"
                                         data-easing="Power4.easeOut"
                                         data-endspeed="300"
                                         data-endeasing="Power1.easeIn" 
                                         data-captionhidden="on"> <img src="img/slider/arrow-bottom-right.png" class="img-responsive arrow" alt="" />
                                    </div>
                                    <!-- Inside LAYERS // bottom-left-circle -->
                                    <div class="tp-caption sfl circle-left"
                                         data-x="center"
                                         data-hoffset="-350"
                                         data-y="center"
                                         data-voffset="120"
                                         data-speed="1000"
                                         data-start="5500"
                                         data-easing="Power4.easeOut"
                                         data-endspeed="300"
                                         data-endeasing="Power1.easeIn" 
                                         data-captionhidden="on"> <div class="caption-item"><i class="icon-gift"></i></div>
                                    </div>
                                    <!-- Inside LAYERS // bottom-left-arrow -->
                                    <div class="tp-caption sfl"
                                         data-x="center"
                                         data-hoffset="-240"
                                         data-y="center"
                                         data-voffset="160"
                                         data-speed="500"
                                         data-start="5800"
                                         data-easing="Power4.easeOut"
                                         data-endspeed="300"
                                         data-endeasing="Power1.easeIn" 
                                         data-captionhidden="on"> <img src="img/slider/arrow-bottom-left.png" class="img-responsive arrow" alt="" />
                                    </div>
                                </li>
                                <!-- SLIDE TWO -->
                                <li data-transition="fade" data-slotamount="7" data-masterspeed="500" >
                                    <!-- MAIN IMAGE -->
                                    <img src="img/slider/2.jpg" alt="slidebg1" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
                                    <!-- LAYERS  -->
                                    <!-- Inside LAYERS // heading -->
                                    <div class="tp-caption very_large_text sft heading"
                                         data-x="center"
                                         data-y="top"
                                         data-voffset="30"
                                         data-speed="700"
                                         data-start="1200"
                                         data-easing="Power4.easeOut"
                                         data-endspeed="300"
                                         data-endeasing="Power1.easeIn" 
                                         data-captionhidden="on"> Special Offer
                                    </div>
                                    <!--Inside LAYERS Image -->
                                    <div class="tp-caption sfl slide-two"
                                         data-x="center"
                                         data-hoffset="-230"
                                         data-y="center"
                                         data-speed="500"
                                         data-start="1500"
                                         data-easing="Power4.easeOut"
                                         data-endspeed="300"
                                         data-endeasing="Power1.easeIn" 
                                         data-captionhidden="on"> <img src="img/slider/s22.png" class="img-responsive" alt="" />
                                    </div>
                                    <!--Inside LAYERS Image -->
                                    <div class="tp-caption sfr slide-two"
                                         data-x="center"
                                         data-hoffset="250"
                                         data-y="center"
                                         data-voffset="30"
                                         data-speed="500"
                                         data-start="2500"
                                         data-easing="Power4.easeOut"
                                         data-endspeed="300"
                                         data-endeasing="Power1.easeIn" 
                                         data-captionhidden="on"> <img src="img/slider/s23.png" class="img-responsive" alt="" />
                                    </div>
                                    <!--Inside LAYERS Image -->
                                    <div class="tp-caption sfb slide-two"
                                         data-x="center"
                                         data-hoffset="30"
                                         data-y="center"
                                         data-voffset="100"
                                         data-speed="500"
                                         data-start="3500"
                                         data-easing="Power4.easeOut"
                                         data-endspeed="300"
                                         data-endeasing="Power1.easeIn" 
                                         data-captionhidden="on"> <img src="img/slider/s21.png" class="img-responsive" alt="" />
                                    </div>
                                    <!--Inside LAYERS Price -->
                                    <div class="tp-caption lft slide-two"
                                         data-x="center"
                                         data-hoffset="-350"
                                         data-y="center"
                                         data-voffset="-70"
                                         data-speed="300"
                                         data-start="1800"
                                         data-easing="Power4.easeOut"
                                         data-endspeed="300"
                                         data-endeasing="Power1.easeIn" 
                                         data-captionhidden="on"> <div class="price-tag">$399</div>
                                    </div>
                                    <!--Inside LAYERS price -->
                                    <div class="tp-caption lft slide-two"
                                         data-x="center"
                                         data-hoffset="250"
                                         data-y="center"
                                         data-voffset="-120"
                                         data-speed="300"
                                         data-start="2800"
                                         data-easing="Power4.easeOut"
                                         data-endspeed="300"
                                         data-endeasing="Power1.easeIn" 
                                         data-captionhidden="on"> <div class="price-tag">$999</div>
                                    </div>
                                    <!--Inside LAYERS price -->
                                    <div class="tp-caption lft slide-two"
                                         data-x="center"
                                         data-hoffset="100"
                                         data-y="center"
                                         data-voffset="120"
                                         data-speed="300"
                                         data-start="3800"
                                         data-easing="Power4.easeOut"
                                         data-endspeed="300"
                                         data-endeasing="Power1.easeIn" 
                                         data-captionhidden="on"> <div class="price-tag">$99</div>
                                    </div>
                                </li>
                                <!-- SLIDE THREE -->

                            </ul>
                        </div>
                    </div>
                    <!-- Revolution Slider End -->

                    <!-- Features starts -->

                    <div class="feature">
                        <div class="container" style="width:95%;">
                            <!-- Header heading -->
                            <table>
                                <tr>
                                    <td style="width:93%">
                                        <input type="text" style="height: 48px; border-radius: 20px;" class="form-control" name="search" placeholder="Type to search..." />
                                    </td>
                                    <td>
                                        <input type="submit" class = "btn" style="padding-right:60px;padding-left:60px; border-radius: 20px; margin-left: 20px; height: 45px;" value="Search" name="Search" />
                                    </td>
                            </table>
                        </div>
                    </div>
                    <!-- featured ends -->
                    <style>
                        #containerx {
                            background-color: #F3EAEA;
                            padding: 5px;
                            border-radius: 10px;
                        }
                        #containerxx{

                        }

                        .boxx {
                            width: 100%;
                            border:1px solid #cacaca; -webkit-border-radius: 3px; -moz-border-radius: 3px;border-radius: 3px;font-size:12px;font-family:arial, helvetica, sans-serif; padding: 10px 10px 10px 10px; text-decoration:none; display:inline-block;text-shadow: -1px -1px 0 rgba(0,0,0,0.3);font-weight:bold;
                            background-color: #E6E6E6; background-image: -webkit-gradient(linear, left top, left bottom, from(#E6E6E6), to(#CCCCCC));
                            background-image: -webkit-linear-gradient(top, #E6E6E6, #CCCCCC);
                            background-image: -moz-linear-gradient(top, #E6E6E6, #CCCCCC);
                            background-image: -ms-linear-gradient(top, #E6E6E6, #CCCCCC);
                            background-image: -o-linear-gradient(top, #E6E6E6, #CCCCCC);
                            background-image: linear-gradient(to bottom, #E6E6E6, #CCCCCC);filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#E6E6E6, endColorstr=#CCCCCC);
                        }

                        .boxx:hover{
                            border:1px solid #b3b3b3;
                            background-color: #cdcdcd; background-image: -webkit-gradient(linear, left top, left bottom, from(#cdcdcd), to(#b3b3b3));
                            background-image: -webkit-linear-gradient(top, #cdcdcd, #b3b3b3);
                            background-image: -moz-linear-gradient(top, #cdcdcd, #b3b3b3);
                            background-image: -ms-linear-gradient(top, #cdcdcd, #b3b3b3);
                            background-image: -o-linear-gradient(top, #cdcdcd, #b3b3b3);
                            background-image: linear-gradient(to bottom, #cdcdcd, #b3b3b3);filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#cdcdcd, endColorstr=#b3b3b3);
                        }
                        #itemx {
                            border-radius: 20px;
                            width: 100%;
                            padding: 3px;
                        }
                        #descx{
                            text-align: center;
                        }
                        .overview > li{
                            width:25%;
                            float:left;
                            display:inline;
                            list-style-type: none;
                            padding-right: 20px;
                        }
                        .boxx {
                            position:relative;
                        }

                        .box-gradientx {
                            position:absolute;
                            width:100%;
                            height:100%;
                            border-radius:10px;
                            -moz-border-radius:10px;
                            -webkit-border-radius:10px;
                            background: -moz-linear-gradient(left, #CCCCCC 0%, #EEEEEE 50%, #EEEEEE 50%, #CCCCCC 100%);
                            background: -webkit-gradient(linear, left top, right top, color-stop(0%,#CCCCCC),color-stop(50%,#EEEEEE), color-stop(50%,#EEEEEE), color-stop(100%,#CCCCCC));
                        }

                    </style>
                    <div class="service">
                        <!-- Header heading -->
                        <h3 class="head">Featured products</h3>
                        <!-- Header paragraph -->
                        <p class="para"></p>
                        <div class="container" id="containerxx">

                            <ul class="overview">
                                <li>
                                    <div class="boxx" id="containerx">

                                        <div >
                                            <div id="contentx " >
                                                <img id="itemx" style="border-radius: 7px;" src="asdf.jpg" />
                                            </div>
                                            <div id ="descx">
                                                <h5>Speed Reading</h5>
                                                <span>Price: $11</span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="boxx" id="containerx">

                                        <div >
                                            <div id="contentx " >
                                                <img id="itemx" style="border-radius: 7px;" src="asdf.jpg" />
                                            </div>
                                            <div id ="descx">
                                                <h5>Speed Reading</h5>
                                                <span>Price: $12</span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="boxx" id="containerx">

                                        <div >
                                            <div id="contentx " >
                                                <img id="itemx" style="border-radius: 7px;" src="asdf.jpg" />
                                            </div>
                                            <div id ="descx">
                                                <h5>Speed Reading</h5>
                                                <span>Price: $13</span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="boxx" id="containerx">

                                        <div>
                                            <div id="contentx " >
                                                <img id="itemx" style="border-radius: 7px;" src="asdf.jpg" />
                                            </div>
                                            <div id ="descx">
                                                <h5>Speed Reading</h5>
                                                <span>Price: $14</span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>

                        </div>
                    </div>

                    <!-- Header paragraph -->




                    <!-- featured ends -->		



                    <!-- service block Start -->


                    <div class="service">
                        <div class="container">
                            <!-- Header heading -->
                            <h3 class="head">Categories</h3>
                            <!-- Header paragraph -->
                            <p class="para"></p>
                            <div class="row">
                                <div class="col-md-3 col-sm-6">
                                    <!-- service item block -->
                                    <div class="service-item">
                                        <!-- service Item top text -->
                                        <div class="s-header s-one animated">
                                            <!-- heading -->
                                            <h6>Restaurants</h6>
                                            <!-- paragraph -->
                                            <p>Eat something tezbazar</p>
                                        </div>
                                        <!-- service Item bottom icon -->
                                        <a href="category.jsp?type=restaurants" class="service-icon"><i class="orange"><img style="width:50%;margin-top: -5px; " src="img/food.png" /></i></a>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6">
                                    <!-- service item block -->
                                    <div class="service-item">
                                        <!-- service Item top text -->
                                        <div class="s-header s-two animated">
                                            <!-- heading -->
                                            <h6>Clothes</h6>
                                            <!-- paragraph -->
                                            <p>Suit up something tezbazar</p>
                                        </div>
                                        <!-- service Item bottom icon -->
                                        <a href="#" class="service-icon"><i class="green"><img src="img/clothes.png" style="width:50%;margin-top: -5px; "/></i></a>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6">
                                    <!-- service item block -->
                                    <div class="service-item">
                                        <!-- service Item top text -->
                                        <div class="s-header s-three animated">
                                            <!-- heading -->
                                            <h6>Gifts</h6>
                                            <!-- paragraph -->
                                            <p>Give gift something tezbazar</p>
                                        </div>
                                        <!-- service Item bottom icon -->
                                        <a href="#" class="service-icon"><i class="red"><img src="img/gift.png" style="width:60%;margin-top: -5px; " /></i></a>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6">
                                    <!-- service item block -->
                                    <div class="service-item">
                                        <!-- service Item top text -->
                                        <div class="s-header s-four animated">
                                            <!-- heading -->
                                            <h6>Technology</h6>
                                            <!-- paragraph -->
                                            <p>Find something tezbazar</p>
                                        </div>
                                        <!-- service Item bottom icon -->
                                        <a href="#" class="service-icon"><i class="purple"><img src="img/tech.png" style="width:50%;margin-top: -5px; " /></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%@include file="/inc/footer.jsp" %>
                </div>
                <!-- Mainbar ends -->

                <!-- service block End -->
                <!-- Footer End -->
                <div class="clearfix"></div>
            </div>
        </div>



        <!-- Scroll to top -->
        <span class="totop"><a href="#"><i class="icon-chevron-up"></i></a></span> 

        <!-- Javascript files -->
        <!-- jQuery -->
        <script src="js/jquery.js"></script>
        <!-- Bootstrap JS -->
        <script src="js/bootstrap.min.js"></script>
        <!-- Revolution Slider JS -->
        <script src="js/jquery.themepunch.revolution.min.js"></script>
        <script src="js/jquery.themepunch.plugins.min.js"></script>
        <!-- jQuery way points -->
        <script src="js/waypoints.min.js"></script>
        <!-- Pretty Photo And Isotope JS -->
        <script type="text/javascript" src="js/isotope.js"></script>
        <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
        <!-- Respond JS for IE8 -->
        <script src="js/respond.min.js"></script>
        <!-- HTML5 Support for IE -->
        <script src="js/html5shiv.js"></script>
        <!-- Custom JS -->
        <script src="js/custom.js"></script>

        <script src="js/jquery.tinycarousel.min.js"></script>

        <!-- This page JS -->
        <script type="text/javascript">

             $(document).ready(function(){
             var basildi=0;
             $("#xxxx").click(function(){
             if(basildi==0){
             $(".mainbar").animate({"margin-left":"7px"},"500");
             $("#xx").delay(500).hide();
             basildi=1;	
             $("#xxxx").html("<i class='icon-chevron-right'></i>");
             }
             else
             {
             $(this).hide().delay(2000).show();
             $("#xx").delay(500).show().css("z-index","0").delay(500).css("z-index","100");
             $(".mainbar").animate({"margin-left":"250px"},"500");
             
             basildi=0;
             $("#xxxx").html("<i class='icon-chevron-left'></i>");
             }
             });
             });

        </script>



    </body>	
</html>