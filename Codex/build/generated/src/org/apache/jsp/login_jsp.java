package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class login_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html>\r\n");
      out.write("\t<head>\r\n");
      out.write("\t\t<!-- Title here -->\r\n");
      out.write("\t\t<title>Login/Register</title>\r\n");
      out.write("\t\t<!-- Description, Keywords and Author -->\r\n");
      out.write("\t\t<meta name=\"description\" content=\"Your description\">\r\n");
      out.write("\t\t<meta name=\"keywords\" content=\"Your,Keywords\">\r\n");
      out.write("\t\t<meta name=\"author\" content=\"ResponsiveWebInc\">\r\n");
      out.write("      \r\n");
      out.write("\t\t<!-- Google web fonts -->\r\n");
      out.write("\t\t<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet' type='text/css'>\r\n");
      out.write("\t\t<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>\r\n");
      out.write("\t\t\r\n");
      out.write("\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\r\n");
      out.write("\t\t\r\n");
      out.write("\t\t<!-- Styles -->\r\n");
      out.write("\t\t<!-- Bootstrap CSS -->\r\n");
      out.write("\t\t<link href=\"css/bootstrap.min.css\" rel=\"stylesheet\">\r\n");
      out.write("\t\t<!-- Slider Revolution CSS -->\r\n");
      out.write("\t\t<link href=\"css/settings.css\" rel=\"stylesheet\">\r\n");
      out.write("\t\t<!--[if IE 8]>\r\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"css/settings-ie8.css\">\r\n");
      out.write("\t\t<![endif]-->\r\n");
      out.write("\t\t<!-- Animate CSS -->\r\n");
      out.write("\t\t<link href=\"css/animate.min.css\" rel=\"stylesheet\">\r\n");
      out.write("\t\t<!-- jQuery prettyPhoto -->\r\n");
      out.write("\t\t<link href=\"css/prettyPhoto.css\" rel=\"stylesheet\">\r\n");
      out.write("\t\t<!-- Font awesome CSS -->\r\n");
      out.write("\t\t<link href=\"css/font-awesome.min.css\" rel=\"stylesheet\">\t\t\r\n");
      out.write("\t\t<!-- Custom CSS -->\r\n");
      out.write("\t\t<link href=\"css/adminstyle.css\" rel=\"stylesheet\">\r\n");
      out.write("\t\t<link href=\"css/style.css\" rel=\"stylesheet\">\r\n");
      out.write("\t\t<!--[if IE]>\r\n");
      out.write("\t\t\t<link rel=\"stylesheet\" type=\"text/css\" href=\"css/adminstyle-ie.css\" />\r\n");
      out.write("\t\t<![endif]-->\r\n");
      out.write("            \r\n");
      out.write("\t\t<!-- Favicon -->\r\n");
      out.write("\t\t<link rel=\"shortcut icon\" href=\"#\">\r\n");
      out.write("\t</head>\r\n");
      out.write("\t\r\n");
      out.write("\t<body id=\"wood-body\">\r\n");
      out.write("\r\n");
      out.write("                        \r\n");
      out.write("                              \r\n");
      out.write("\t\r\n");
      out.write("         \r\n");
      out.write("      <div class=\"out-container\">\r\n");
      out.write("         \r\n");
      out.write("\t\t <div class=\"login-page\">\r\n");
      out.write("\t\t\t<div class=\"container\">\r\n");
      out.write("\r\n");
      out.write("\t\t\t\t<!-- Nav tabs -->\r\n");
      out.write("\t\t\t\t<ul class=\"nav nav-tabs nav-justified\">\r\n");
      out.write("\t\t\t\t  <li class=\"active\"><a href=\"#login\" data-toggle=\"tab\"><i class=\"icon-signin\"></i> Login</a></li>\r\n");
      out.write("\t\t\t\t  <li><a href=\"#register\" data-toggle=\"tab\"><i class=\"icon-pencil\"></i> Register</a></li>\r\n");
      out.write("\t\t\t\t  <li><a href=\"#contact\" data-toggle=\"tab\"><i class=\"icon-envelope\"></i> Contact</a></li>\r\n");
      out.write("\t\t\t\t</ul>\r\n");
      out.write("\r\n");
      out.write("\t\t\t\t<!-- Tab panes -->\r\n");
      out.write("\t\t\t\t<div class=\"tab-content\">\r\n");
      out.write("                                    <div class=\"tab-pane fade ");
if(request.getAttribute("action")==null||request.getAttribute("action")==""){
      out.write(" active ");
}
      out.write(" %> in\" id=\"login\">\r\n");
      out.write("\t\t\t\t  \r\n");
      out.write("\t\t\t\t  \r\n");
      out.write("\t\t\t\t\t<!-- Login form -->\r\n");
      out.write("                                        ");
 if (request.getAttribute("loginMessage")!=null&&request.getAttribute("loginMessage")!=""){ 
      out.write("\r\n");
      out.write("\t\t\t\t\t<div class=\"alert alert-warning\">");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.loginMessage}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</div>");
}
      out.write("\r\n");
      out.write("\t\t\t\t\t<form role=\"form\" method = \"post\" action=\"loginServlet\">\r\n");
      out.write("\t\t\t\t\t  <div class=\"form-group\">\r\n");
      out.write("\t\t\t\t\t\t<label for=\"email\">Email</label>\r\n");
      out.write("\t\t\t\t\t\t<input type=\"text\" name = \"email\" class=\"form-control\" id=\"email\" placeholder=\"Email\">\r\n");
      out.write("\t\t\t\t\t  </div>\r\n");
      out.write("\t\t\t\t\t  <div class=\"form-group\">\r\n");
      out.write("\t\t\t\t\t\t<label for=\"password\">Password</label>\r\n");
      out.write("\t\t\t\t\t\t<input type=\"password\" name=\"password\" class=\"form-control\" id=\"password\" placeholder=\"Password\">\r\n");
      out.write("\t\t\t\t\t  </div>\r\n");
      out.write("\t\t\t\t\t  <div class=\"checkbox\">\r\n");
      out.write("\t\t\t\t\t\t<label>\r\n");
      out.write("\t\t\t\t\t\t  <input type=\"checkbox\"> Remember Me Next Time\r\n");
      out.write("\t\t\t\t\t\t</label>\r\n");
      out.write("\t\t\t\t\t  </div>\r\n");
      out.write("\t\t\t\t\t  <button type=\"submit\" class=\"btn btn-danger btn-sm\">Submit</button>\r\n");
      out.write("\t\t\t\t\t  <button type=\"reset\" class=\"btn btn-black btn-sm\">Reset</button>\r\n");
      out.write("\t\t\t\t\t</form>\r\n");
      out.write("\t\t\t\t\t\r\n");
      out.write("\t\t\t\t  </div>\r\n");
      out.write("\t\t\t\t  \r\n");
      out.write("\t\t\t\t  \r\n");
      out.write("\t\t\t\t  <div class=\"tab-pane fade ");
if(request.getAttribute("action")=="register"){
      out.write(" active ");
}
      out.write(" \" id=\"register\">\r\n");
      out.write("\t\t\t\t\t<!-- Register form -->\r\n");
      out.write("\t\t\t\t\t\r\n");
      out.write("\t\t\t\t\t<form role=\"form\" method=\"post\" action=\"controllerServlet?action=register\">\r\n");
      out.write("                                              ");
 if (request.getAttribute("registerMessage")!=null&&request.getAttribute("registerMessage")!=""){ 
      out.write("\r\n");
      out.write("\t\t\t\t\t<div class=\"alert alert-warning\">");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.registerMessage}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</div>");
}
      out.write("\r\n");
      out.write("                                              <div class=\"form-group\">\r\n");
      out.write("                                                  <table>\r\n");
      out.write("                                                <tr>\r\n");
      out.write("                                                    <td><label for=\"name\">Name</label>\r\n");
      out.write("\t\t\t\t\t\t<input type=\"text\" name=\"name\" class=\"form-control\" id=\"name\" placeholder=\"Name\"></td>\r\n");
      out.write("                                                    <td id=\"surnameTd\"><label for=\"name\">Surname</label>\r\n");
      out.write("\t\t\t\t\t\t<input type=\"text\" name=\"surname\" class=\"form-control\" id=\"name\" placeholder=\"Surname\"></td>\r\n");
      out.write("                                                </tr>\r\n");
      out.write("                                            </table>\r\n");
      out.write("\t\t\t\t\t\r\n");
      out.write("\t\t\t\t\t\t\r\n");
      out.write("\t\t\t\t\t  </div>  \r\n");
      out.write("<!--                                            <div class=\"form-group\">\r\n");
      out.write("\r\n");
      out.write("                                          </div>-->\r\n");
      out.write("\t\t\t\t\t  <div class=\"form-group\">\r\n");
      out.write("\t\t\t\t\t\t<label for=\"email\">Email</label>\r\n");
      out.write("\t\t\t\t\t\t<input type=\"text\" name=\"email\" class=\"form-control\" id=\"email\" placeholder=\"Email\">\r\n");
      out.write("\t\t\t\t\t  </div>\r\n");
      out.write("\t\t\t\t\t  <div class=\"form-group\">\r\n");
      out.write("\t\t\t\t\t\t<label for=\"password\">Password</label>\r\n");
      out.write("\t\t\t\t\t\t<input type=\"password\" name=\"password\" class=\"form-control\" id=\"password\" placeholder=\"Password\">\r\n");
      out.write("\t\t\t\t\t  </div>\r\n");
      out.write("\t\t\t\t\t  <div class=\"form-group\">\r\n");
      out.write("\t\t\t\t\t\t<label for=\"cpassword\">Confirm Password</label>\r\n");
      out.write("\t\t\t\t\t\t<input type=\"cpassword\" name=\"confirmPassword\" class=\"form-control\" id=\"cpassword\" placeholder=\"Confirm Password\">\r\n");
      out.write("\t\t\t\t\t  </div>\r\n");
      out.write("\t\t\t\t\t  <div class=\"checkbox\">\r\n");
      out.write("\t\t\t\t\t\t<label>\r\n");
      out.write("\t\t\t\t\t\t  <input type=\"checkbox\" name=\"terms\"> Agree <a href=\"#\">Terms & Conditions</a>\r\n");
      out.write("\t\t\t\t\t\t</label>\r\n");
      out.write("\t\t\t\t\t  </div>\r\n");
      out.write("\t\t\t\t\t  <button type=\"submit\" class=\"btn btn-danger btn-sm\">Submit</button>\r\n");
      out.write("\t\t\t\t\t  <button type=\"reset\" class=\"btn btn-black btn-sm\">Reset</button>\r\n");
      out.write("\t\t\t\t\t</form>\r\n");
      out.write("\t\t\t\t  \r\n");
      out.write("\t\t\t\t  </div>\r\n");
      out.write("\t\t\t\t  \r\n");
      out.write("\t\t\t\t  \r\n");
      out.write("\t\t\t\t  <div class=\"tab-pane fade ");
if(request.getAttribute("action")!=null&&request.getAttribute("action")=="contact"){
      out.write(" active ");
}
      out.write(" \" id=\"contact\">\t\t\t\t\t\r\n");
      out.write("\t\t\t\t\t<!-- Contact Form -->\r\n");
      out.write("\t\t\t\t\t\r\n");
      out.write("\t\t\t\t\t<form role=\"form\" method = \"post\" action=\"controllerServlet?action=contact\">\r\n");
      out.write("                                              ");
 if (request.getAttribute("contactMessage")!=null&&request.getAttribute("contactMessage")!=""){ 
      out.write("\r\n");
      out.write("\t\t\t\t\t<div class=\"alert alert-warning\">");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.contactMessage}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</div>");
}
      out.write("\r\n");
      out.write("\t\t\t\t\t  <div class=\"form-group\">\r\n");
      out.write("\t\t\t\t\t\t<label for=\"name\">Name</label>\r\n");
      out.write("\t\t\t\t\t\t<input type=\"text\" name=\"name\" class=\"form-control\" id=\"name\" placeholder=\"Name\">\r\n");
      out.write("\t\t\t\t\t  </div>\r\n");
      out.write("\t\t\t\t\t  <div class=\"form-group\">\r\n");
      out.write("\t\t\t\t\t\t<label for=\"email\">Email</label>\r\n");
      out.write("\t\t\t\t\t\t<input type=\"text\" name=\"email\" class=\"form-control\" id=\"email\" placeholder=\"Email\">\r\n");
      out.write("\t\t\t\t\t  </div>\r\n");
      out.write("\t\t\t\t\t  <div class=\"form-group\">\r\n");
      out.write("\t\t\t\t\t\t<label for=\"message\">Message</label>\r\n");
      out.write("\t\t\t\t\t\t<textarea rows=\"3\" name=\"message\" class=\"form-control\"></textarea>\r\n");
      out.write("\t\t\t\t\t  </div>\r\n");
      out.write("\t\t\t\t\t  <button type=\"submit\" class=\"btn btn-danger btn-sm\">Submit</button>\r\n");
      out.write("\t\t\t\t\t  <button type=\"reset\" class=\"btn btn-black btn-sm\">Reset</button>\r\n");
      out.write("\t\t\t\t\t</form>\r\n");
      out.write("\t\t\t\t\t\r\n");
      out.write("\t\t\t\t  </div>\r\n");
      out.write("\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t\r\n");
      out.write("\t\t\t</div>\r\n");
      out.write("\t\t </div>\t\r\n");
      out.write("\t\t \r\n");
      out.write("      </div>\r\n");
      out.write("\t\t\r\n");
      out.write("      \r\n");
      out.write("\t  <!-- Javascript files -->\r\n");
      out.write("\t  <!-- jQuery -->\r\n");
      out.write("\t  <script src=\"js/jquery.js\"></script>\r\n");
      out.write("\t  <!-- Bootstrap JS -->\r\n");
      out.write("\t  <script src=\"js/bootstrap.min.js\"></script>\r\n");
      out.write("\t  <!-- Revolution Slider JS -->\r\n");
      out.write("\t  <script src=\"js/jquery.themepunch.revolution.min.js\"></script>\r\n");
      out.write("\t  <script src=\"js/jquery.themepunch.plugins.min.js\"></script>\r\n");
      out.write("\t  <!-- jQuery way points -->\r\n");
      out.write("\t  <script src=\"js/waypoints.min.js\"></script>\r\n");
      out.write("\t  <!-- Pretty Photo And Isotope JS -->\r\n");
      out.write("\t  <script type=\"text/javascript\" src=\"js/isotope.js\"></script>\r\n");
      out.write("\t  <script type=\"text/javascript\" src=\"js/jquery.prettyPhoto.js\"></script>\r\n");
      out.write("\t  <!-- Respond JS for IE8 -->\r\n");
      out.write("\t  <script src=\"js/respond.min.js\"></script>\r\n");
      out.write("\t  <!-- HTML5 Support for IE -->\r\n");
      out.write("\t  <script src=\"js/html5shiv.js\"></script>\r\n");
      out.write("\t  <!-- Custom JS -->\r\n");
      out.write("\t  <script src=\"js/custom.js\"></script>\r\n");
      out.write("\t  \r\n");
      out.write("\t  <!-- This page JS -->\r\n");
      out.write("\t  <script type=\"text/javascript\">\r\n");
      out.write("\t\t\t//code here....\r\n");
      out.write("\t  </script>\r\n");
      out.write("            \r\n");
      out.write("\t</body>\t\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
