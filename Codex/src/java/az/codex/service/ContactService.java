/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package az.codex.service;

import az.codex.database.LoginDao;
import az.codex.domain.Contact;

/**
 *
 * @author hikmet
 */
public interface ContactService 
{
    public boolean contact(Contact contactInfo);
}
