/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package az.codex.service;

import az.codex.database.ContactDao;
import az.codex.domain.Contact;

/**
 *
 * @author hikmet
 */
public class DefaultContactService implements ContactService
{
   private ContactDao contactDao;

    public DefaultContactService(ContactDao ContactDao)
    {
        this.contactDao = ContactDao;
    }
   
    
    @Override
    public boolean contact(Contact contactInfo)
    {
       return  contactDao.contact(contactInfo);
    }
    
}
