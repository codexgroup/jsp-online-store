/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package az.codex.service;

import az.codex.database.LoginDao;
import az.codex.domain.Admin;
import az.codex.domain.User;


public class DefaultLoginService implements LoginService {
    
    private LoginDao loginDao;
    
    public DefaultLoginService(LoginDao loginDao) {
        this.loginDao = loginDao;
    }
    
    @Override
    public User login(String email, String password) {
        return loginDao.login(email, password);
    }    
    
    public Admin loginAdmin(String email, String password) {
        return loginDao.adminLogin(email, password);
    } 
}
