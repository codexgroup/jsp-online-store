/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package az.codex.service;

import az.codex.database.RegistrationDao;
import az.codex.domain.RegistrationForm;


public class DefaultRegistrationService implements RegistrationService {

    private RegistrationDao registrationDao;
    
    public DefaultRegistrationService(RegistrationDao registrationDao) {
        this.registrationDao = registrationDao;
    }   
    
    @Override
    public boolean isUniqueEmail(String email) {
        return registrationDao.isUniqueEmail(email);
    }

    @Override
    public boolean register(RegistrationForm form,String confirmString) {
        return registrationDao.register(form,confirmString);
    }       
    
}
