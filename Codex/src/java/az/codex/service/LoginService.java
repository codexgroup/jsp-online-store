/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package az.codex.service;

import az.codex.domain.Admin;
import az.codex.domain.User;


public interface LoginService {
    User login(String email, String password);
    
    Admin loginAdmin(String email,String password);
}
