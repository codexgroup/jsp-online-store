/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package az.codex.database;

import az.codex.domain.RegistrationForm;


public interface RegistrationDao {
    
    public boolean isUniqueEmail(String email);
    
    public boolean register(RegistrationForm form,String confirmString);
    
}
