/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package az.codex.database;

import az.codex.domain.Admin;
import az.codex.domain.User;


public interface LoginDao {
    User login(String email, String password); 
    Admin adminLogin(String email, String password);
    
}
