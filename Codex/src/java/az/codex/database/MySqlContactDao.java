/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package az.codex.database;

import az.codex.domain.Contact;
import az.codex.utility.EncryptionUtility;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author hikmet
 */
public class MySqlContactDao implements ContactDao
{

    private DataSource ds;
    public MySqlContactDao(DataSource ds)
    {
       this.ds = ds;
    }
     
    @Override
    public boolean contact(Contact contactInfo)
    {
         boolean success = false;
         Connection con = null;
         PreparedStatement ps = null;
        
        try {
            
            con = ds.getConnection();
            ps = con.prepareStatement(SqlQuery.CONTACT);
            ps.setString(1, contactInfo.getName());
            ps.setString(2, contactInfo.getEmail());
            ps.setString(3, contactInfo.getMessage());
            ps.setDate(4, (Date) contactInfo.getSendDate());
            int count = ps.executeUpdate();
            if(count == 1) {
                success = true;
            }
            con.commit();
        } catch(Exception e) {  
           e.printStackTrace();
        } finally {
            DatabaseUtility.close(null, ps, con);
        }
        return success;    
    }
    
}
