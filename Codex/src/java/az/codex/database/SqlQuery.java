/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package az.codex.database;


public class SqlQuery {
    public static final String LOGIN = "SELECT * FROM user WHERE email=? AND password=?" ;
    
    public static final String CHECK_EMAIL = "SELECT * FROM user WHERE email=? ";
    
    public static final String REGISTER_USER = "INSERT INTO user(name,surname,email,password,confirm_string,registration_date,status) VALUES(?,?,?,?,?,?,?)";
    
    public static final String VERIFY = "SELECT * FROM user WHERE id=?";
    
    public static final String CHECK_ADMIN = "SELECT * FROM admin WHERE id=?";
    
    public static final String GET_STORES = "SELECT * FROM store WHERE company_id=?";
    
    //public static final String ADD_CLOTHES="INSERT INTO clothes+products (store_id, brand_name,category,price,season,modified_date) VALUES (?,?,?,?,?,?)";
    
    //public static final String GET_PRODUCTS
    
    public static final String CONTACT = "";
}
