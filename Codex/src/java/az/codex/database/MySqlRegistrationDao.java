/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package az.codex.database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import az.codex.domain.RegistrationForm;
import az.codex.domain.Status;
import az.codex.utility.EncryptionUtility;

public class MySqlRegistrationDao implements RegistrationDao {

    private DataSource dataSource;
    
    public MySqlRegistrationDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }
    
    @Override
    public boolean isUniqueEmail(String email) {
        boolean unique = true;
        
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            con = dataSource.getConnection();
            ps = con.prepareStatement(SqlQuery.CHECK_EMAIL);
            ps.setString(1, email);
            rs = ps.executeQuery();
            if(rs.next()) {
                unique = false;
            }            
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            DatabaseUtility.close(rs, null, con);
        }
        
        return unique;
    }

    @Override
    public boolean register(RegistrationForm form,String confirmString) {
        boolean success = false;
        Connection con = null;
        PreparedStatement ps = null;
        try {
            
            java.util.Date utilDate = new java.util.Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
            con = dataSource.getConnection();
            ps = con.prepareStatement(SqlQuery.REGISTER_USER);
            ps.setString(1, form.getName());
            ps.setString(2, form.getSurname());
            ps.setString(3, form.getEmail());
            ps.setString(4, EncryptionUtility.sha1(form.getPassword()));
            ps.setString(5, confirmString);
            ps.setDate(6,sqlDate);
            ps.setObject(7, 0);
            int count = ps.executeUpdate();
            if(count == 1) {
                success = true;
            }
        } catch(Exception e) {
           
            e.printStackTrace();
           
        } finally {
            DatabaseUtility.close(null, ps, con);
        }
        
        return success;        
    }
    
}
