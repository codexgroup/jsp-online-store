/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package az.codex.database;

import az.codex.domain.Admin;
import az.codex.domain.Store;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.sql.DataSource;
import az.codex.domain.User;
import java.util.ArrayList;
import java.util.List;


public class MySqlLoginDao implements LoginDao {

    private DataSource dataSource;
    
    public MySqlLoginDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }
    
    @Override
    public User login(String email, String password) {
        
        User user = null;
        
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            con = dataSource.getConnection();
            ps = con.prepareStatement(SqlQuery.LOGIN);
            ps.setString(1, email);
            ps.setString(2, password);
            rs = ps.executeQuery();
            if(rs.next()) {
                user = new User();
                user.setId(rs.getLong("id"));
                user.setName(rs.getString("name"));
                user.setSurname(rs.getString("surname"));
                user.setEmail(rs.getString("email"));
                user.setLastLogin(rs.getDate("last_login_date"));
            }            
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            DatabaseUtility.close(rs, ps, con);
        }
        
        return user;
        
    }
    
    public Admin adminLogin(String email, String password) {
        
        Admin admin = null;
        
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            con = dataSource.getConnection();
            ps = con.prepareStatement(SqlQuery.LOGIN);
            ps.setString(1, email);
            ps.setString(2, password);
            rs = ps.executeQuery();
            if(rs.next()) {
                admin= new Admin();
                admin.setId(rs.getLong("id"));
                ps = con.prepareStatement(SqlQuery.CHECK_ADMIN);
                ps.setLong(1, admin.getId());
                rs=ps.executeQuery();
                if(rs.next())
                {
                   admin.setName(rs.getString("name"));
                   admin.setSurname(rs.getString("surname"));
                   admin.setEmail(rs.getString("email"));
                   admin.setLastLogin(rs.getDate("last_login_date")); 
                   admin.setCompanyId(rs.getInt("company_id"));
                   ps = con.prepareStatement(SqlQuery.GET_STORES);
                   rs=ps.executeQuery();
                   List<Store> stores = new ArrayList<>();
                   if(rs.next())
                   {
                     Store store = new Store();
                     store.setId(rs.getInt("id"));
                     store.setName(rs.getString("name"));
                     store.setAddress(rs.getString("address"));
                     store.setPhoneNumber("phone_number");
                   }
                }
            }            
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            DatabaseUtility.close(rs, ps, con);
        }
        
        return admin;
        
    }
}
