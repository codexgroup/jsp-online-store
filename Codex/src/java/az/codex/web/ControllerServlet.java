/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package az.codex.web;

import az.codex.database.ContactDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import az.codex.database.DatabaseUtility;
import az.codex.database.MySqlContactDao;
import az.codex.database.MySqlRegistrationDao;
import az.codex.database.RegistrationDao;
import az.codex.domain.Contact;
import az.codex.domain.RegistrationForm;
import az.codex.service.ContactService;
import az.codex.service.DefaultContactService;
import az.codex.service.DefaultRegistrationService;
import az.codex.service.RegistrationService;
import az.codex.utility.EncryptionUtility;
import java.net.URLEncoder;
import java.util.Date;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.MultiPartEmail;
import org.apache.commons.mail.SimpleEmail;
import org.apache.commons.validator.GenericValidator;

public class ControllerServlet extends HttpServlet
{

    private RegistrationForm readRegistrationData(HttpServletRequest request)
    {
        RegistrationForm form = new RegistrationForm();

        if (request.getParameter("name") != null)
        {
            form.setName(request.getParameter("name").trim());
        }

        if (request.getParameter("surname") != null)
        {
            form.setSurname(request.getParameter("surname").trim());
        }

        if (request.getParameter("email") != null)
        {
            form.setEmail(request.getParameter("email").trim());
        }

        if (request.getParameter("password") != null)
        {
            form.setPassword(request.getParameter("password").trim());
        }

        if (request.getParameter("confirmPassword") != null)
        {
            form.setPasswordConfirmation(request.getParameter("confirmPassword").trim());
        }

        /*   if(request.getParameter("captcha") != null) {
         form.setCaptcha(request.getParameter("captcha").trim
         }*/
        return form;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try
        {

            DataSource ds = DatabaseUtility.connect();

            HttpSession session = request.getSession();
            String action = "";

            if (request.getParameter("action") != null)
            {
                action = request.getParameter("action").trim();
            }

            if (action.equals("register"))
            {
                // Action plan
                // 1.read registration form data
                // 2.validate form data
                // 3.process registration business logic
                // 4.redirect/forward user to required page

                //1.read registration form data
                RegistrationDao registrationDao = new MySqlRegistrationDao(ds);
                RegistrationService registrationService = new DefaultRegistrationService(registrationDao);
                RegistrationForm form = readRegistrationData(request);

                Map<String, List<String>> errors = ValidationUtility.validateRegistrationForm(form);

                if (!registrationService.isUniqueEmail(form.getEmail()))
                {
                    errors.get("email").add("Email must be unique");
                }

                boolean isValidForm = true;

                for (String key : errors.keySet())
                {
                    List<String> validationErrors = errors.get(key);
                    if (!validationErrors.isEmpty())
                    {
                        isValidForm = false;
                        break;
                    }
                }

                // 3.process registration business logic
                // 3a.if data is valid, continue registration processing
                // 3b.save reg data and redirect user to registration page, show errors
                if (isValidForm)
                {
                    String confirmString;
                   
                       // if(!confirmString.startsWith("'") && !confirmString.startsWith("\"") && !confirmString.startsWith("\\") $$ !confirmString.startsWith("'") && !confirmString.startsWith("\"") && !confirmString.startsWith("\\"))
                        confirmString = EncryptionUtility.sha1(form.getName()+form.getSurname()+form.getEmail()+form.getPassword());
                        confirmString = URLEncoder.encode(confirmString, "UTF-8");
                        String link = "<html><body><a href='http://localhost:8084/Codex/verifyServlet?id="+confirmString+"'"+">THIS IS LINK</a></body></html>";

                    boolean success = registrationService.register(form,confirmString);
                    String registrationResult = "";
                    if (success)
                    {
                         registrationResult = "You've successfully registered";
                         
                        /* Email email = new SimpleEmail();
                         EmailAttachment attachment = new EmailAttachment();
                         email.setHostName("smtp.gmail.com");
                         email.setAuthentication("hikmetqurbanli@gmail.com", "1*H@h*1q");
                         email.setSSLOnConnect(true);
                         email.setStartTLSEnabled(true);
                         email.setFrom("hikmetqurbanli@gmail.com");
                         email.setSubject("TestMail");
                         email.setMsg("<a href='http://localhost:8084/Codex/verifyServlet?id=fdghfdsghf>This is email</a>"); 
                         email.addTo("hikmet_qurbanli@mail.ru");
                         email.send();*/
                         
                         HtmlEmail email = new HtmlEmail();
                         email.setHostName("smtp.gmail.com");
                         email.setAuthentication("hikmetqurbanli@gmail.com", "1*H@h*1q");
                         email.setSSLOnConnect(true);
                         email.setStartTLSEnabled(true);
                         email.addTo("hikmet_qurbanli@mail.ru");
                         email.setFrom("hikmetqurbanli@gmail.com");
                         email.setSubject("This is verification email");
                         email.setHtmlMsg(link);
                         email.setTextMsg("This will be expired after 24 hours");
                         email.send();

                    } else
                    {
                        registrationResult = "Your registration failed. Please try again or contact support";
                    }

                    RequestDispatcher rd = request.getRequestDispatcher("profile.jsp");
                    request.setAttribute("message", registrationResult);
                    rd.forward(request, response);
                } else
                {
                    // 3b.save reg data and redirect user to registration page, show errors
                    session.setAttribute("registrationErrors", errors);
                    session.setAttribute("registrationForm", form);
//                    response.sendRedirect("register.jsp");
                    request.setAttribute("action", "register");
                    RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
                    rd.forward(request, response);
                }

            } else if (action.equals("contact"))
            {

                String answer;
                String name = request.getParameter("name").trim();
                String email = request.getParameter("email").trim();
                String message = request.getParameter("message").trim();

                if (GenericValidator.isEmail(email))
                {
                    if (name != null && name.length() > 0 && email != null && email.length() > 0 && message != null && message.length() > 0)
                    {
                        ContactDao contactDao = new MySqlContactDao(ds);
                        ContactService contactService = new DefaultContactService(contactDao);
                        Contact contactInfo = new Contact(name, email, message, new Date());
                        boolean success = contactService.contact(contactInfo);
                        if (success)
                        {
                            answer = "Your will be answered soon :)";
                        } else
                        {
                            answer = "Message failed,try again";
                        }
                    } else
                    {
                        answer = "Please fill the required fields";
                    }
                } else
                {
                    answer = "Email is invalid";
                }

                request.setAttribute("answer", answer);
            }
            
        } catch (Exception e)
        {
            e.printStackTrace();
        } finally
        {
            out.close();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }
}
