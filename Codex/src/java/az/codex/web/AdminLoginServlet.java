/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package az.codex.web;

import az.codex.database.DatabaseUtility;
import az.codex.database.LoginDao;
import az.codex.database.MySqlLoginDao;
import az.codex.domain.User;
import az.codex.service.DefaultLoginService;
import az.codex.service.LoginService;
import az.codex.utility.EncryptionUtility;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

/**
 *
 * @author hikmet
 */
public class AdminLoginServlet extends HttpServlet
{

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter())
        {
            String email = request.getParameter("email");
            String password = request.getParameter("password");
             if(email != null && email.trim().length() > 0 && 
                    password != null && password.trim().length() > 0) {
                email = email.trim();
                password = password.trim(); 
                DataSource ds = DatabaseUtility.connect();
                LoginDao loginDao = new MySqlLoginDao(ds);
                LoginService loginService = new DefaultLoginService(loginDao);
                User user = loginService.login(email, EncryptionUtility.sha1(password));
                HttpSession session = request.getSession();
                
                if(user != null) 
                {                    
                    session.setAttribute("user", user);
                    session.setAttribute("loginTime", new Date());
                    if(user.getLastLogin()!=null)
                    {
                        response.sendRedirect("index.jsp");
                    }
                    else
                    {
                       response.sendRedirect("profile.jsp");
                    }
                    
                    System.out.println("user " + user.getName() + " successfully logged in");
                } else {
                    System.out.println("Invalid email and password. user " + email);                    
                    response.sendRedirect("login.jsp");
                }
                                
            } else {
                request.setAttribute("message", "Invalid email or password");
                response.sendRedirect("login.jsp");
            }
        } catch (Exception ex)
        {
            Logger.getLogger(AdminLoginServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */


    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>

}
