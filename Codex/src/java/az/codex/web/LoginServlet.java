/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package az.codex.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import az.codex.database.DatabaseUtility;
import az.codex.database.LoginDao;
import az.codex.database.MySqlLoginDao;
import az.codex.domain.User;
import az.codex.service.DefaultLoginService;
import az.codex.service.LoginService;
import az.codex.utility.EncryptionUtility;

public class LoginServlet extends HttpServlet {

  
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String email = request.getParameter("email");
            String password = request.getParameter("password");
            
            if(email != null && email.trim().length() > 0 && 
                    password != null && password.trim().length() > 0) {
                email = email.trim();
                password = password.trim();
                
                DataSource ds = DatabaseUtility.connect();
                LoginDao loginDao = new MySqlLoginDao(ds);
                LoginService loginService = new DefaultLoginService(loginDao);
                User user = loginService.login(email, EncryptionUtility.sha1(password));
                HttpSession session = request.getSession();
                
                if(user != null) 
                {             
                    long id = user.getId();
                    session.setAttribute("user", user);
                    session.setAttribute("loginTime", new Date());
                    if(user.getLastLogin()!=null)
                    {
                        response.sendRedirect("index.jsp");
                    }
                    else
                    {
                       response.sendRedirect("profile.jsp");
                    }
                    
                    System.out.println("user " + user.getName() + " successfully logged in");
                } else {
                    System.out.println("Invalid email and password. user " + email);                    
                    response.sendRedirect("login.jsp");
                }
                                
            } else {
                request.setAttribute("message", "Invalid email or password");
                response.sendRedirect("login.jsp");
            }
        } catch(Exception e) {  
            e.printStackTrace();
            response.sendRedirect("error.jsp");
        } finally {            
            out.close();
        }
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }   
}
