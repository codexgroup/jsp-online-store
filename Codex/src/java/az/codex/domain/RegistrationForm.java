/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package az.codex.domain;


public class RegistrationForm {
    private String name;
    private String surname;
    private String email;
    private String password;
    private String passwordConfirmation;
  //  private String captcha;

    public RegistrationForm()
    {
        this.name = "";
        this.surname = "";
        this.email = "";
        this.password = "";
        this.passwordConfirmation = "";
    }

    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    /*public String getCaptcha() {
        return captcha;
    }

    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }
    
    */
    
}
