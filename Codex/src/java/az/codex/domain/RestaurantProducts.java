/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package az.codex.domain;


public class RestaurantProducts extends BaseDomain
{
    private int storeId;
    private String name;
    private float price;
    private String imageUrl;
    private float discount;
    private String description;
    private String modifiedDate;
    private int cusineId;

    public RestaurantProducts() 
    {
        this.storeId = 0;
        this.name = "";
        this.price = 0;
        this.imageUrl = "";
        this.discount = 0;
        this.description = "";
        this.modifiedDate = "";
        this.cusineId = 0;
    }

    public int getStoreId()
    {
        return storeId;
    }

    public void setStoreId(int storeId)
    {
        this.storeId = storeId;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public float getPrice()
    {
        return price;
    }

    public void setPrice(float price)
    {
        this.price = price;
    }

    public String getImageUrl()
    {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl)
    {
        this.imageUrl = imageUrl;
    }

    public float getDiscount()
    {
        return discount;
    }

    public void setDiscount(float discount)
    {
        this.discount = discount;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getModifiedDate()
    {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate)
    {
        this.modifiedDate = modifiedDate;
    }

    public int getCusineId()
    {
        return cusineId;
    }

    public void setCusineId(int cusineId)
    {
        this.cusineId = cusineId;
    }
    
    
    
    
    
    
}
