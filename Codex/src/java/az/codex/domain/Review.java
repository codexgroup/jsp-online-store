/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package az.codex.domain;


public class Review extends BaseDomain
{
    private int reviewerId;
    private String reviewDate;
    private String reviewContent;
    private int stars;
    private int reviewedItemId;

    public Review()
    {
        this.reviewerId = 0;
        this.reviewDate = "";
        this.reviewContent = "";
        this.stars = 0;
        this.reviewedItemId = 0;
    }

    public int getReviewerId()
    {
        return reviewerId;
    }

    public void setReviewerId(int reviewerId)
    {
        this.reviewerId = reviewerId;
    }

    public String getReviewDate()
    {
        return reviewDate;
    }

    public void setReviewDate(String reviewDate)
    {
        this.reviewDate = reviewDate;
    }

    public String getReviewContent()
    {
        return reviewContent;
    }

    public void setReviewContent(String reviewContent)
    {
        this.reviewContent = reviewContent;
    }

    public int getStars()
    {
        return stars;
    }

    public void setStars(int stars)
    {
        this.stars = stars;
    }

    public int getReviewedItemId()
    {
        return reviewedItemId;
    }

    public void setReviewedItemId(int reviewedItemId)
    {
        this.reviewedItemId = reviewedItemId;
    }

    @Override
    public String toString()
    {
        return "Review{" + "reviewerId=" + reviewerId + ", reviewDate=" + reviewDate + ", reviewContent=" + reviewContent + ", stars=" + stars + ", reviewedItemId=" + reviewedItemId + '}';
    }
    
    
}
