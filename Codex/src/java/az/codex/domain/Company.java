/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package az.codex.domain;


public class Company extends BaseDomain
{
    private String name;
    private String address;
    private String email;
    private String website;
    private String phoneNumber;
    private String mobileNumber;
    private String facebookUrl;

    public Company()
    {
        this.name = "";
        this.address = "";
        this.email = "";
        this.website = "";
        this.phoneNumber = "";
        this.mobileNumber = "";
        this.facebookUrl="";
    }

    public String getFacebookUrl()
    {
        return facebookUrl;
    }

    public void setFacebookUrl(String facebookUrl)
    {
        this.facebookUrl = facebookUrl;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getWebsite()
    {
        return website;
    }

    public void setWebsite(String website)
    {
        this.website = website;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public String getMobileNumber()
    {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber)
    {
        this.mobileNumber = mobileNumber;
    }

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public Status getStatus()
    {
        return status;
    }

    public void setStatus(Status status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "Company{" + "name=" + name + ", address=" + address + ", email=" + email + ", website=" + website + ", phoneNumber=" + phoneNumber + ", mobileNumber=" + mobileNumber + ", facebookUrl=" + facebookUrl + '}';
    }
    
    
    
}
