/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package az.codex.domain;

import java.util.Date;


public class User extends BaseDomain {
    
    protected String name;
    protected String surname;
    protected String password;
    protected String email;
    protected String address;
    protected String mobileNumber;
    protected String facebookProfile;
    protected String homeCoordinates;
    protected String birthday;
    protected String gender;
    protected Date lastLogin;

    public User()
    {
        this.name = "";
        this.surname = "";
        this.password = "";
        this.email = "";
        this.address = "";
        this.mobileNumber = "";
        this.facebookProfile = "";
        this.homeCoordinates = "";
        this.birthday = "";
        this.gender = "";
    }
    

    public Date getLastLogin()
    {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin)
    {
        this.lastLogin = lastLogin;
    }
    
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getSurname()
    {
        return surname;
    }

    public void setSurname(String surname)
    {
        this.surname = surname;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getMobileNumber()
    {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber)
    {
        this.mobileNumber = mobileNumber;
    }

    public String getFacebookProfile()
    {
        return facebookProfile;
    }

    public void setFacebookProfile(String facebookProfile)
    {
        this.facebookProfile = facebookProfile;
    }

    public String getHomeCoordinates()
    {
        return homeCoordinates;
    }

    public void setHomeCoordinates(String homeCoordinates)
    {
        this.homeCoordinates = homeCoordinates;
    }

    public String getBirthday()
    {
        return birthday;
    }

    public void setBirthday(String birthday)
    {
        this.birthday = birthday;
    }

    public String getGender()
    {
        return gender;
    }

    public void setGender(String gender)
    {
        this.gender = gender;
    }

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public Status getStatus()
    {
        return status;
    }

    public void setStatus(Status status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "User{" + "name=" + name + ", surname=" + surname + ", password=" + password + ", email=" + email + ", address=" + address + ", mobileNumber=" + mobileNumber + ", facebookProfile=" + facebookProfile + ", homeCoordinates=" + homeCoordinates + ", birthday=" + birthday + ", gender=" + gender + '}';
    }
    
    
}
