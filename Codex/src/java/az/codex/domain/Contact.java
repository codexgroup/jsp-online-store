/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package az.codex.domain;

import java.util.Date;
import javax.naming.Name;
import org.apache.commons.validator.EmailValidator;

/**
 *
 * @author hikmet
 */
public class Contact extends BaseDomain
{
    private String Name;
    private String email;
    private String message;
    private Date sendDate;

    public Contact(String Name, String email, String message,Date sendDate)
    {
        this.Name = Name;
        this.email = email;
        this.message = message;
        this.sendDate = sendDate;
    }

    public Date getSendDate()
    {
        return sendDate;
    }

    public void setSendDate(Date sendDate)
    {
        this.sendDate = sendDate;
    }

    public String getName()
    {
        return Name;
    }

    public void setName(String Name)
    {
        this.Name = Name;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public Status getStatus()
    {
        return status;
    }

    public void setStatus(Status status)
    {
        this.status = status;
    }
    
    
}
