/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package az.codex.domain;


public class Admin extends User
{
    private int companyId;
    private int isSuperAdmin;

    public Admin()
    {
        this.companyId = 0;
        this.isSuperAdmin = -1;
    }

    public int getCompanyId()
    {
        return companyId;
    }

    public void setCompanyId(int companyId)
    {
        this.companyId = companyId;
    }

    public int getIsSuperAdmin()
    {
        return isSuperAdmin;
    }

    public void setIsSuperAdmin(int isSuperAdmin)
    {
        this.isSuperAdmin = isSuperAdmin;
    }
    
    
    
  
   
   
    
}



