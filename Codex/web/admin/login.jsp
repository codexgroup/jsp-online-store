
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<!-- Title here -->
		<title>Login/Register</title>
		<!-- Description, Keywords and Author -->
		<meta name="description" content="Your description">
		<meta name="keywords" content="Your,Keywords">
		<meta name="author" content="ResponsiveWebInc">
      
		<!-- Google web fonts -->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<!-- Styles -->
		<!-- Bootstrap CSS -->
		<link href="../css/bootstrap.min.css" rel="stylesheet">
		<!-- Slider Revolution CSS -->
		<link href="../css/settings.css" rel="stylesheet">
		<!--[if IE 8]>
		<link rel="stylesheet" href="../css/settings-ie8.css">
		<![endif]-->
		<!-- Animate CSS -->
		<link href="../css/animate.min.css" rel="stylesheet">
		<!-- jQuery prettyPhoto -->
		<link href="../css/prettyPhoto.css" rel="stylesheet">
		<!-- Font awesome CSS -->
		<link href="../css/font-awesome.min.css" rel="stylesheet">		
		<!-- Custom CSS -->
		<link href="../css/adminstyle.css" rel="stylesheet">
		<link href="../css/style.css" rel="stylesheet">
		<!--[if IE]>
			<link rel="stylesheet" type="text/css" href="../css/adminstyle-ie.css" />
		<![endif]-->
            
		<!-- Favicon -->
		<link rel="shortcut icon" href="#">
	</head>
	
	<body id="wood-body">

                        
                              
	
         
      <div class="out-container">
         
		 <div class="login-page">
			<div class="container">

				<!-- Nav tabs -->
				<ul class="nav nav-tabs nav-justified">
				  <li class="active"><a href="#login" data-toggle="tab"><i class="icon-signin"></i> Login</a></li>
				  <li><a href="#register" data-toggle="tab"><i class="icon-pencil"></i> Register</a></li>
				  <li><a href="#contact" data-toggle="tab"><i class="icon-envelope"></i> Contact</a></li>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">
                                    <div class="tab-pane fade <%if(request.getAttribute("action")==null||request.getAttribute("action")==""){%> active <%}%> %> in" id="login">
				  
				  
					<!-- Login form -->
                                        <% if (request.getAttribute("loginMessage")!=null&&request.getAttribute("loginMessage")!=""){ %>
					<div class="alert alert-warning">${requestScope.loginMessage}</div><%}%>
					<form role="form" method = "post" action="index.jsp">
					  <div class="form-group">
						<label for="email">Email</label>
						<input type="text" name = "email" class="form-control" id="email" placeholder="Email">
					  </div>
					  <div class="form-group">
						<label for="password">Password</label>
						<input type="password" name="password" class="form-control" id="password" placeholder="Password">
					  </div>
					  <div class="checkbox">
						<label>
						  <input type="checkbox"> Remember Me Next Time
						</label>
					  </div>
					  <button type="submit" class="btn btn-danger btn-sm">Submit</button>
					  <button type="reset" class="btn btn-black btn-sm">Reset</button>
					</form>
					
				  </div>
				  
				  
				  <div class="tab-pane fade <%if(request.getAttribute("action")=="register"){%> active <%}%> " id="register">
					<!-- Register form -->
					
					<form role="form" method="post" action="controllerServlet?action=register">
                                              <% if (request.getAttribute("registerMessage")!=null&&request.getAttribute("registerMessage")!=""){ %>
					<div class="alert alert-warning">${requestScope.registerMessage}</div><%}%>
                                              <div class="form-group">
                                                  <table>
                                                <tr>
                                                    <td><label for="name">Name</label>
						<input type="text" name="name" class="form-control" id="name" placeholder="Name"></td>
                                                    <td id="surnameTd"><label for="name">Surname</label>
						<input type="text" name="surname" class="form-control" id="name" placeholder="Surname"></td>
                                                </tr>
                                            </table>
					
						
					  </div>  
<!--                                            <div class="form-group">

                                          </div>-->
					  <div class="form-group">
						<label for="email">Email</label>
						<input type="text" name="email" class="form-control" id="email" placeholder="Email">
					  </div>
					  <div class="form-group">
						<label for="password">Password</label>
						<input type="password" name="password" class="form-control" id="password" placeholder="Password">
					  </div>
					  <div class="form-group">
						<label for="cpassword">Confirm Password</label>
						<input type="password" name="confirmPassword" class="form-control" id="cpassword" placeholder="Confirm Password">
					  </div>
					  <div class="checkbox">
						<label>
						  <input type="checkbox" name="terms"> Agree <a href="#">Terms & Conditions</a>
						</label>
					  </div>
					  <button type="submit" class="btn btn-danger btn-sm">Submit</button>
					  <button type="reset" class="btn btn-black btn-sm">Reset</button>
					</form>
				  
				  </div>
				  
				  
				  <div class="tab-pane fade <%if(request.getAttribute("action")!=null&&request.getAttribute("action")=="contact"){%> active <%}%> " id="contact">					
					<!-- Contact Form -->
					
					<form role="form" method = "post" action="controllerServlet?action=contact">
                                              <% if (request.getAttribute("contactMessage")!=null&&request.getAttribute("contactMessage")!=""){ %>
					<div class="alert alert-warning">${requestScope.contactMessage}</div><%}%>
					  <div class="form-group">
						<label for="name">Name</label>
						<input type="text" name="name" class="form-control" id="name" placeholder="Name">
					  </div>
					  <div class="form-group">
						<label for="email">Email</label>
						<input type="text" name="email" class="form-control" id="email" placeholder="Email">
					  </div>
					  <div class="form-group">
						<label for="message">Message</label>
						<textarea rows="3" name="message" class="form-control"></textarea>
					  </div>
					  <button type="submit" class="btn btn-danger btn-sm">Submit</button>
					  <button type="reset" class="btn btn-black btn-sm">Reset</button>
					</form>
					
				  </div>
				</div>
				
			</div>
		 </div>	
		 
      </div>
		
      
	  <!-- Javascript files -->
	  <!-- jQuery -->
	  <script src="js/jquery.js"></script>
	  <!-- Bootstrap JS -->
	  <script src="js/bootstrap.min.js"></script>
	  <!-- Revolution Slider JS -->
	  <script src="js/jquery.themepunch.revolution.min.js"></script>
	  <script src="js/jquery.themepunch.plugins.min.js"></script>
	  <!-- jQuery way points -->
	  <script src="js/waypoints.min.js"></script>
	  <!-- Pretty Photo And Isotope JS -->
	  <script type="text/javascript" src="js/isotope.js"></script>
	  <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
	  <!-- Respond JS for IE8 -->
	  <script src="js/respond.min.js"></script>
	  <!-- HTML5 Support for IE -->
	  <script src="js/html5shiv.js"></script>
	  <!-- Custom JS -->
	  <script src="js/custom.js"></script>
	  
	  <!-- This page JS -->
	  <script type="text/javascript">
			//code here....
	  </script>
            
	</body>	
</html>