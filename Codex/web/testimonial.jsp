<!DOCTYPE html>
<html>
	<head>
		<!-- Title here -->
		<title>Sheldon - Testimonial</title>
		<!-- Description, Keywords and Author -->
		<meta name="description" content="Your description">
		<meta name="keywords" content="Your,Keywords">
		<meta name="author" content="ResponsiveWebInc">
      
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<!-- Google web fonts -->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,600' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
		
		<!-- Styles -->
		<!-- Bootstrap CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<!-- Slider Revolution CSS -->
		<link href="css/settings.css" rel="stylesheet">
		<!--[if IE 8]>
		<link rel="stylesheet" href="css/settings-ie8.css">
		<![endif]-->
		<!-- Animate CSS -->
		<link href="css/animate.min.css" rel="stylesheet">
		<!-- jQuery prettyPhoto -->
		<link href="css/prettyPhoto.css" rel="stylesheet">
		<!-- Font awesome CSS -->
		<link href="css/font-awesome.min.css" rel="stylesheet">		
		<!-- Custom CSS -->
		<link href="css/style.css" rel="stylesheet">
		<!--[if IE]>
			<link rel="stylesheet" type="text/css" href="css/style-ie.css" />
		<![endif]-->
            
		<!-- Favicon -->
		<link rel="shortcut icon" href="#">
	</head>
	
	<body>


      <div class="out-container">
         <div class="outer">
            <!-- Sidebar starts -->
            <%@include file="/inc/sidebar.jsp" %>
            <!-- Sidebar ends -->
            
            <!-- Mainbar starts -->
            <div class="mainbar">
               
			   
				<!-- Inner page start -->
			   
				<div class="inner-page">
					<!-- Inner page headder -->
					<div class="inner-page-header">
						<div class="container">
							<!-- Header inner -->
							<div class="header-inner">
								<!-- Heading -->
								<h2>Testimonial</h2>
								<!-- Paragraph -->
								<p><span class="red">&#47;&#47;</span> Sed ut perspiciatis of Lorem Ipsum.</p>
							</div>
						</div>
					</div>
							
					<!-- inner page content start -->
					<div class="inner-page-content inner-testimonial">
						<div class="container">
							<div class="row">
								<div class="col-md-4">
									<!-- Testimonial item -->
									<div class="testimonial-item">
										<!-- User Image -->
										<img src="img/user-big.jpg" class="img-responsive img-thumbnail img-circle" alt="" />
										<!-- User name and designation -->
										<h4>H. Natus<span>, CEO</span></h4>
										<!-- User comments -->
										<blockquote>
											<p>Sed ut perspiciatis unde omnis iste natust error sit vol accusantium doloremque and laudantium, totam rem aperiam, eaque an ipsa quae abe illo explicabo. An oiuk nesciunt.</p>
										</blockquote>
									</div>
								</div>
								<div class="col-md-4">
									<!-- Testimonial item -->
									<div class="testimonial-item">
										<!-- User Image -->
										<img src="img/user-big.jpg" class="img-responsive img-thumbnail img-circle" alt="" />
										<!-- User name and designation -->
										<h4>Jon Asse<span>, MD</span></h4>
										<!-- User comments -->
										<blockquote>
											<p>Sed ut perspiciatis unde omnis iste natust error sit vol accusantium doloremque and laudantium, totam rem aperiam, eaque an ipsa quae abe illo explicabo. An oiuk nesciunt.</p>
										</blockquote>
									</div>
								</div>
								<div class="col-md-4">
									<!-- Testimonial item -->
									<div class="testimonial-item">
										<!-- User Image -->
										<img src="img/user-big.jpg" class="img-responsive img-thumbnail img-circle" alt="" />
										<!-- User name and designation -->
										<h4>Tas Mend<span>, CA</span></h4>
										<!-- User comments -->
										<blockquote>
											<p>Sed ut perspiciatis unde omnis iste natust error sit vol accusantium doloremque and laudantium, totam rem aperiam, eaque an ipsa quae abe illo explicabo. An oiuk nesciunt.</p>
										</blockquote>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<!-- Testimonial item -->
									<div class="testimonial-item">
										<!-- User Image -->
										<img src="img/user-big.jpg" class="img-responsive img-thumbnail img-circle" alt="" />
										<!-- User name and designation -->
										<h4>R. Gheos<span>, CA II</span></h4>
										<!-- User comments -->
										<blockquote>
											<p>Sed ut perspiciatis unde omnis iste natust error sit vol accusantium doloremque and laudantium, totam rem aperiam, eaque an ipsa quae abe illo explicabo. An oiuk nesciunt.</p>
										</blockquote>
									</div>
								</div>
								<div class="col-md-4">
									<!-- Testimonial item -->
									<div class="testimonial-item">
										<!-- User Image -->
										<img src="img/user-big.jpg" class="img-responsive img-thumbnail img-circle" alt="" />
										<!-- User name and designation -->
										<h4>Den Louse<span>, FM</span></h4>
										<!-- User comments -->
										<blockquote>
											<p>Sed ut perspiciatis unde omnis iste natust error sit vol accusantium doloremque and laudantium, totam rem aperiam, eaque an ipsa quae abe illo explicabo. An oiuk nesciunt.</p>
										</blockquote>
									</div>
								</div>
								<div class="col-md-4">
									<!-- Testimonial item -->
									<div class="testimonial-item">
										<!-- User Image -->
										<img src="img/user-big.jpg" class="img-responsive img-thumbnail img-circle" alt="" />
										<!-- User name and designation -->
										<h4>Oues Lenn<span>, CA III</span></h4>
										<!-- User comments -->
										<blockquote>
											<p>Sed ut perspiciatis unde omnis iste natust error sit vol accusantium doloremque and laudantium, totam rem aperiam, eaque an ipsa quae abe illo explicabo. An oiuk nesciunt.</p>
										</blockquote>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- inner page content end -->
							
				</div>
			   
				<!-- Inner page end -->
			    
				
				<!-- CTA Start -->
				
				<div class="cta">
					<div class="container">
						<!-- CTA content block -->
						<div class="cta-content">
							<div class="row">
								<div class="col-md-2">
									<!-- cta flat icon image -->
									<img src="img/5.png" class="img-responsive" alt="" />
								</div>
								<div class="col-md-7">
									<!-- cta heading -->
									<h3><strong>Nor again</strong> is there anyone</h3>
									<!-- cta paragraph -->
									<p>Nam libero tempore, cum soluta nobis est eligendi optwer of choice is untrammelled and when nothing prevents our being able to do what we like best quod possimus.</p>
								</div>
								<div class="col-md-3">
									<!-- cta button link -->
									<a href="#" class="btn btn-info"><i class="icon-cloud"></i>&nbsp; Download Now</a><br /><a href="#" class="btn btn-danger"><i class="icon-hand-right"></i>&nbsp; Try Now</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<!-- CTA End -->
				
				<!-- Social media start -->
				
				<div class="social-media">
					<div class="container">
						<!-- socila inner block -->
						<div class="social-inner">
							<!-- Socila list -->
							<ul>
								<li>
									<a href="#" class="social-item facebook">
										<!-- social media icon-->
										<i class="icon-facebook"></i>
										<!-- Title -->
										<span class="title">Facebook</span>
										<!-- Related Information -->
										<span class="sinfo">2,345 Likes</span>
									</a>
								</li>
								<li>
									<a href="#" class="social-item twitter">
										<!-- social media icon-->
										<i class="icon-twitter"></i>
										<!-- Title -->
										<span class="title">Twitter</span>
										<!-- Related Information -->
										<span class="sinfo">follow 145</span>
									</a>
								</li>
								<li>
									<a href="#" class="social-item google">
										<!-- social media icon-->
										<i class="icon-google-plus"></i>
										<!-- Title -->
										<span class="title">Google</span>
										<!-- Related Information -->
										<span class="sinfo">95 Groups</span>
									</a>
								</li>
								<li>
									<a href="#" class="social-item linkedin">
										<!-- social media icon-->
										<i class="icon-linkedin"></i>
										<!-- Title -->
										<span class="title">LinkedIn</span>
										<!-- Related Information -->
										<span class="sinfo">2,345 Likes</span>
									</a>
								</li>
							</ul>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
				
				<!-- Social media end -->
				
				
				<!-- Footer Start -->
		
				<div class="footer">
					<div class="container">
						<!-- Footer content -->
						<div class="footer-content">
							<div class="row">
								<div class="col-md-3 col-xs-12">
									<div class="footer-widget">
										<!-- Heading -->
										<h5><span><i class="icon-puzzle-piece"></i></span> Navigation Menu</h5>
										<!-- Navigate menu -->
										<ul class="menu">
											<!-- Menu list -->
											<li><a href="#"><i class="icon-home"></i> Home</a></li>
											<li><a href="#"><i class="icon-book"></i> Careers</a></li>
											<li><a href="#"><i class="icon-picture"></i> Gallery</a></li>
											<li><a href="#"><i class="icon-group"></i> Testimonial</a></li>
										</ul>
									</div>
								</div>
								<div class="col-md-3 col-xs-12">
									<div class="footer-widget">
										<!-- Heading -->
										<h5><span><i class="icon-phone"></i></span> Contact Us</h5>
										<!-- Email and Phone number -->
										<p><i class="icon-envelope"></i> <a href="#">abcd@example.com</a><br />
											<i class="icon-phone"></i> +91-1234567899</p>
										<!-- Social icons -->
										<div class="social">
										  <a href="#" class="facebook"><i class="icon-facebook"></i></a>
										  <a href="#" class="twitter"><i class="icon-twitter"></i></a>
										  <a href="#" class="google-plus"><i class="icon-google-plus"></i></a>
										  <a href="#" class="linkedin"><i class="icon-linkedin"></i></a>
									   </div>
									</div>
								</div>
								<div class="col-md-3 col-xs-12">
									<div class="footer-widget">
										<!-- Heading -->
										<h5><span><i class="icon-gears"></i></span> Dolor Sitamet</h5>
										<!-- Navigate menu -->
										<ul>
											<li><a href="#">Sed do eiusmod</a></li>
											<li><a href="#">Excepteur sint occaeca</a></li>
											<li><a href="#">Nemo enim ipsam</a></li>
											<li><a href="#">Voluptatem accusantium</a></li>
										</ul>
									</div>
								</div>
								<div class="col-md-3 col-xs-12">
									<div class="footer-widget">
										<!-- Heading -->
										<h5><span><i class="icon-envelope"></i></span> Subscribe Now</h5>
										<p>At vero eos et accus odiusantiuo dign amus et iustod.</p>
										<!-- form -->
										<form>
											<div class="input-group">
												<input type="text" class="form-control" placeholder="Email">
												<span class="input-group-btn">
													<button class="btn btn-info" type="button"><i class="icon-envelope"></i></button>
												</span>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
						<!-- Footer Copyright -->
						<div class="footer-copyright">
							<p>&copy; Copyright 2013 - <a href="#">Company Name</a></p>
						</div>
					</div>
				</div>
				
				<!-- Footer End -->
			   
            </div>
            <!-- Mainbar ends -->
            
            <div class="clearfix"></div>
         </div>
      </div>
		
		
      
      <!-- Scroll to top -->
      <span class="totop"><a href="#"><i class="icon-chevron-up"></i></a></span> 
		
	  <!-- Javascript files -->
	  <!-- jQuery -->
	  <script src="js/jquery.js"></script>
	  <!-- Bootstrap JS -->
	  <script src="js/bootstrap.min.js"></script>
	  <!-- Revolution Slider JS -->
	  <script src="js/jquery.themepunch.revolution.min.js"></script>
	  <script src="js/jquery.themepunch.plugins.min.js"></script>
	  <!-- jQuery way points -->
	  <script src="js/waypoints.min.js"></script>
	  <!-- Pretty Photo And Isotope JS -->
	  <script type="text/javascript" src="js/isotope.js"></script>
	  <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
	  <!-- Respond JS for IE8 -->
	  <script src="js/respond.min.js"></script>
	  <!-- HTML5 Support for IE -->
	  <script src="js/html5shiv.js"></script>
	  <!-- Custom JS -->
	  <script src="js/custom.js"></script>
	  
	  <!-- This page JS -->
	  <script type="text/javascript">
			//code here....
	  </script>
      
      
	</body>	
</html>