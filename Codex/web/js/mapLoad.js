$(document).ready(function(){

	 var map;
   var mapOptions;
   var markers = [];
function initialize() {
  mapOptions = {
          center: new google.maps.LatLng(40.371594, 49.8441),
          zoom: 8
        };
  map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
  
  google.maps.event.addListener(map, 'click', function(event) {
      if(tempMarker)
      {
        tempMarker.setMap(null);
      }
      tempMarker = placeMarker(event.latLng);
  });
}
 google.maps.event.addDomListener(window, 'load', initialize);
function placeMarker(location) {

      var marker = new google.maps.Marker({
      position: location,
      map: map
     });
      return marker;
}
});