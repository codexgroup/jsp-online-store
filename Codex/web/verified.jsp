<%-- 
    Document   : Verified
    Created on : Jan 26, 2014, 8:53:31 PM
    Author     : hikmet
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        <c:if test="${requestScope.verified}">
        <p>Your account verified successfully! <a href = "login.jsp">Login</a><p>
        </c:if>
        
         <c:if test="${requestScope.verified==false}">
        <p>Your account has been expired or you haven't registered yet.Here you can register  <a href = "login.jsp">Login</a><p>
        </c:if>
            
            
          
    </body>
</html>
