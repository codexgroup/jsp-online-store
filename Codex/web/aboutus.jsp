<!DOCTYPE html>
<html>
	<head>
		<!-- Title here -->
		<title>Sheldon - About Us</title>
		<!-- Description, Keywords and Author -->
		<meta name="description" content="Your description">
		<meta name="keywords" content="Your,Keywords">
		<meta name="author" content="ResponsiveWebInc">
      
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<!-- Google web fonts -->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,600' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
		
		<!-- Styles -->
		<!-- Bootstrap CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<!-- Slider Revolution CSS -->
		<link href="css/settings.css" rel="stylesheet">
		<!--[if IE 8]>
		<link rel="stylesheet" href="css/settings-ie8.css">
		<![endif]-->
		<!-- Animate CSS -->
		<link href="css/animate.min.css" rel="stylesheet">
		<!-- jQuery prettyPhoto -->
		<link href="css/prettyPhoto.css" rel="stylesheet">
		<!-- Font awesome CSS -->
		<link href="css/font-awesome.min.css" rel="stylesheet">		
		<!-- Custom CSS -->
		<link href="css/style.css" rel="stylesheet">
		<!--[if IE]>
			<link rel="stylesheet" type="text/css" href="css/style-ie.css" />
		<![endif]-->
            
		<!-- Favicon -->
		<link rel="shortcut icon" href="#">
	</head>
	
	<body>


      <div class="out-container">
         <div class="outer">
            <!-- Sidebar starts -->
            <%@include file="/inc/sidebar.jsp" %>
            <!-- Sidebar ends -->
            
            <!-- Mainbar starts -->
            <div class="mainbar">
               
			   
					<div class="inner-page">
					<!-- Inner page headder -->
					<div class="inner-page-header">
						<div class="container">
							<!-- Header inner -->
							<div class="header-inner">
								<!-- Heading -->
								<h2>About Us</h2>
								<!-- Paragraph -->
								
							</div>
						</div>
					</div>
					<!-- inner page content start -->
					<div class="inner-page-content contact-us">
						<div class="container">
							<div class="row">
								<div class="col-md-6">	
									<!-- contact us address -->
									<div class="contact-address">
										<!-- Heading -->
										<h5>About Us</h5>
										
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- inner page content end -->
				</div>
	
				
				
				<!-- Footer Start -->
		
                    <%@include file="/inc/footer.jsp" %>
				<!-- Footer End -->
			   
            </div>
            <!-- Mainbar ends -->
            
            <div class="clearfix"></div>
         </div>
      </div>
		
		
      
      <!-- Scroll to top -->
      <span class="totop"><a href="#"><i class="icon-chevron-up"></i></a></span> 
		
	   <!-- Javascript files -->
	  <!-- jQuery -->
	  <script src="js/jquery.js"></script>
	  <!-- Bootstrap JS -->
	  <script src="js/bootstrap.min.js"></script>
	  <!-- Revolution Slider JS -->
	  <script src="js/jquery.themepunch.revolution.min.js"></script>
	  <script src="js/jquery.themepunch.plugins.min.js"></script>
	  <!-- jQuery way points -->
	  <script src="js/waypoints.min.js"></script>
	  <!-- Pretty Photo And Isotope JS -->
	  <script type="text/javascript" src="js/isotope.js"></script>
	  <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
	  <!-- Respond JS for IE8 -->
	  <script src="js/respond.min.js"></script>
	  <!-- HTML5 Support for IE -->
	  <script src="js/html5shiv.js"></script>
	  <!-- Custom JS -->
	  <script src="js/custom.js"></script>
	  
	  <!-- This page JS -->
	  <script type="text/javascript">
			//code here....
	  </script>
      
      
	</body>	
</html>