<%-- 
    Document   : test_admin
    Created on : Jan 27, 2014, 12:33:44 PM
    Author     : Cavid
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add product</title>
    </head>
    <body>
        <form method="post" action="AddProduct" enctype="multipart/form-data">
            store_id: <input type="text" name="store_id"><br/>
            brand_name: <input type="text" name="brand_name"><br/>
            category: <input type="text" name="category"><br/>
            price: <input type="text" name="price"><br/>
            season: <input type="text" name="season"><br/>
            Image(Select file to upload):<input type="file" name="dataFile" id="fileChooser"/><br/><br/>
            <input type="submit" value="Add" />
        </form>
    </body>
</html>
