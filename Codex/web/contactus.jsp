<!DOCTYPE html>
<html>
	<head>
		<!-- Title here -->
		<title>Sheldon - Contact Us</title>
		<!-- Description, Keywords and Author -->
		<meta name="description" content="Your description">
		<meta name="keywords" content="Your,Keywords">
		<meta name="author" content="ResponsiveWebInc">
      
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<!-- Google web fonts -->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,600' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
		
		<!-- Styles -->
		<!-- Bootstrap CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<!-- Slider Revolution CSS -->
		<link href="css/settings.css" rel="stylesheet">
		<!--[if IE 8]>
		<link rel="stylesheet" href="css/settings-ie8.css">
		<![endif]-->
		<!-- Animate CSS -->
		<link href="css/animate.min.css" rel="stylesheet">
		<!-- jQuery prettyPhoto -->
		<link href="css/prettyPhoto.css" rel="stylesheet">
		<!-- Font awesome CSS -->
		<link href="css/font-awesome.min.css" rel="stylesheet">		
		<!-- Custom CSS -->
		<link href="css/style.css" rel="stylesheet">
		<!--[if IE]>
			<link rel="stylesheet" type="text/css" href="css/style-ie.css" />
		<![endif]-->
            
		<!-- Favicon -->
		<link rel="shortcut icon" href="#">
	</head>
	
	<body>


      <div class="out-container">
         <div class="outer">
            <!-- Sidebar starts -->
            <%@include file="/inc/sidebar.jsp"%>
            <!-- Sidebar ends -->
            
            <!-- Mainbar starts -->
            <div class="mainbar">
               
			   
					<!-- inner page content start -->
					<div class="inner-page-content contact-us">
						<div class="container">
							<div class="row">
								<div class="col-md-6">	
									<div class="contact-map">
										<!-- Googlr map -->
										<iframe width="100%" height="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.in/?ie=UTF8&amp;ll=12.953997,77.63094&amp;spn=0.781586,1.352692&amp;t=m&amp;z=10&amp;output=embed"></iframe>
									</div>
									<!-- contact us address -->
									<div class="contact-address">
										<!-- Heading -->
										<h5>Contact Us</h5>
										<div class="row">
											<div class="col-md-6">
												<!-- Icon / Address -->
												<i class="icon-map-marker"></i>&nbsp;<span>Qafqaz Universiteti</span>
												<!-- Icon / Email -->
												<i class="icon-envelope"></i>&nbsp;<span><a href="#">Helelik emailimiz yoxdu</a></span>
												<!-- Icon / Phone number -->
												<i class="icon-phone"></i>&nbsp;<span>Helelik nomremiz de yoxdu</span>
												<div class="clearfix"></div>
											</div>
											<div class="col-md-6">
												<!-- Sub heading -->
												<h6>Business Hours</h6>
												<!-- Icon / Time / Day -->
												<i class="icon-time"></i>&nbsp;<span>Monday - Sunday</span>
												<i class="icon-time"></i>&nbsp;<span>Seher - Axsam</span>
												<div class="clearfix"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<!-- Contact Form Area -->
									<div class="contact-form">
										<h5>Contact Form</h5>
										<form class="form-horizontal" role="form">
											<div class="form-group">
												<label for="name" class="col-lg-2 control-label">Name</label>
												<div class="col-lg-10">
													<!-- form input -->
													<input type="text" class="form-control" id="name" placeholder="Name">
												</div>
											</div>
											<div class="form-group">
												<label for="email" class="col-lg-2 control-label">Email</label>
												<div class="col-lg-10">
													<!-- form input -->
													<input type="email" class="form-control" id="email" placeholder="Email">
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-lg-2" for="comment">Comment</label>
												<div class="col-lg-10">
													<!-- form textarea -->
												  <textarea class="form-control" id="comment" rows="3" placeholder="Comments"></textarea>
												</div>
											</div>
											  
											<div class="form-group">
												<div class="col-lg-offset-2 col-lg-10">
													<!-- form Submit and reset button -->
													<button type="submit" class="btn btn-info">Submit</button> &nbsp;
													<button type="reset" class="btn">Reset</button>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- inner page content end -->
	
				<!-- Footer Start -->
		
                    <%@include file="/inc/footer.jsp" %>
				
				<!-- Footer End -->
			   
            </div>
            <!-- Mainbar ends -->
            
            <div class="clearfix"></div>
         </div>
      </div>
		
		
      
      <!-- Scroll to top -->
      <span class="totop"><a href="#"><i class="icon-chevron-up"></i></a></span> 
		
	  <!-- Javascript files -->
	  <!-- jQuery -->
	  <script src="js/jquery.js"></script>
	  <!-- Bootstrap JS -->
	  <script src="js/bootstrap.min.js"></script>
	  <!-- Revolution Slider JS -->
	  <script src="js/jquery.themepunch.revolution.min.js"></script>
	  <script src="js/jquery.themepunch.plugins.min.js"></script>
	  <!-- jQuery way points -->
	  <script src="js/waypoints.min.js"></script>
	  <!-- Pretty Photo And Isotope JS -->
	  <script type="text/javascript" src="js/isotope.js"></script>
	  <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
	  <!-- Respond JS for IE8 -->
	  <script src="js/respond.min.js"></script>
	  <!-- HTML5 Support for IE -->
	  <script src="js/html5shiv.js"></script>
	  <!-- Custom JS -->
	  <script src="js/custom.js"></script>
	  
	  <!-- This page JS -->
	  <script type="text/javascript">
			//code here....
	  </script>
      
      
	</body>	
</html>