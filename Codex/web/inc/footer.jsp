
                    <div class="footer">
                        <div class="container">
                            <!-- Footer content -->
                            <div class="footer-content">
                                <div class="row">
                                    <div class="col-md-3 col-xs-12">
                                        <div class="footer-widget">
                                            <!-- Heading -->
                                            <h5><span><i class="icon-puzzle-piece"></i></span> Navigation Menu</h5>
                                            <!-- Navigate menu -->
                                            <ul class="menu">
                                                <!-- Menu list -->
                                                <li><a href="index.jsp"><i class="icon-home"></i> Home</a></li>
                                                <li><a href="category.jsp"><i class="icon-tasks"></i> Category</a></li>
                                                <li><a href="profile.jsp"><i class="icon-user"></i> Profile</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-xs-12">
                                        <div class="footer-widget">
                                            <!-- Heading -->
                                            <h5><span><i class="icon-phone"></i></span> Contact Us</h5>
                                            <!-- Email and Phone number -->
                                            <p><i class="icon-envelope"></i> <a href="#">Helelik emailimiz yoxdu</a><br />
                                                <i class="icon-phone"></i> Helelik telefonumuz da yoxdu</p>
                                            <!-- Social icons -->
                                            
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-xs-12">
                                        <div class="footer-widget">
                                            <!-- Heading -->
                                            <h5><span><i class="icon-gears"></i></span> Categories</h5>
                                            <!-- Navigate menu -->
                                            <ul>
                                                <!-- navigation menu item -->
                                                <li><a href="/category.jsp?id=1">Restaurant</a></li>
                                                <li><a href="/category.jsp?id=1">Fashion</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-xs-12">
                                        <div class="footer-widget">
                                            <!-- Heading -->
                                            <h5><span><i class="icon-envelope"></i></span> Subscribe Now</h5>
                                            <p>Email islemir...</p>
                                            <!-- form -->
                                            <form>
                                                <div class="input-group">
                                                    <!-- form input -->
                                                    <input type="text" class="form-control" placeholder="Email">
                                                    <span class="input-group-btn">
                                                        <!-- join button -->
                                                        <button class="btn btn-info" type="button"><i class="icon-envelope"></i></button>
                                                    </span>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Footer Copyright -->
                            <div class="footer-copyright">
                                <p>&copy; Copyright 2013 - <a href="#">Codex Software</a></p>
                            </div>
                        </div>
                    </div>