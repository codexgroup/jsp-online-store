<%-- 
    Document   : sidebar
    Created on : Jan 25, 2014, 9:34:08 PM
    Author     : hikmet
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

            <div id="xx" class="sidebar" style="z-index:100">
                  <!-- Logo starts -->
                  <div class="logo">
                      <a href="index.jsp"><img style="width:100%" src="/img/logo.png"></img></a></h1>
                  </div>
                  <!-- Logo ends -->
                  <!-- Sidebar buttons starts -->
                  <div class="sidebar-buttons text-center" style="padding-bottom: 10px;">
                     <!-- User button -->
                     <%if(session.getAttribute("user")!=null){%>
                     <div class="btn-group">
                       <a href="profile.jsp" class="btn btn-black btn-xs"><i class="icon-user"></i></a>
                       <a href="profile.jsp" class="btn btn-danger btn-xs">User</a>
                     </div>
                     <!-- Lock button -->
                     <div class="btn-group">
                       <a href="lock.jsp" class="btn btn-black btn-xs"><i class="icon-lock"></i></a>
                       <a href="lock.jsp" class="btn btn-danger btn-xs">Lock</a>
                     </div>
                     <%}%>
                     <!-- Logout button -->
                     <div class="btn-group">
                       <a href="login.jsp" class="btn btn-black btn-xs"><i class="icon-off"></i></a>
                       <a href="login.jsp" class="btn btn-danger btn-xs"><%if(session.getAttribute("user")!=null){%>Logout <%}else{%>Login<%}%></a>
                     </div>
                  </div>
                  <!-- Sidebar buttons ends -->
                  
                  <!-- Sidebar navigation starts -->
				  
				  <div class="sidebar-dropdown"><a href="#">Navigation</a></div>
				  
                  <div class="sidey">
                     <ul class="nav">
                         <!-- Main navigation. Refer Notes.txt files for reference. -->
                         
                         <!-- Use the class "current" in main menu to hightlight current main menu -->
                         <li class="current"><a href="index.jsp"><i class="icon-home"></i> Home</a></li>
                         <li class="has_submenu">
                           <a href="#">
                              <i class="icon-bug"></i> Stores <span class="label label-darky">3</span>
                              <span class="caret pull-right"></span>
                           </a>
                             <ul>
                                 <li><a href="store.jsp?id=1"><i class="icon-double-angle-right"></i> Restaurant N1</a></li>
                                 <li><a href="store.jsp?id=2"><i class="icon-double-angle-right"></i> Restaurant N2</a></li>
                                 <li><a href="store.jsp?id=3"><i class="icon-double-angle-right"></i> Clothes Shop N1</a></li>
                             </ul>
                         </li>
                                                  
                     </ul>               
                  </div>
                  <!-- Sidebar navigation ends -->
                  <!-- Sidebar status ends -->
            </div>