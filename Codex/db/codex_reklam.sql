CREATE DATABASE  IF NOT EXISTS `reklam` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `reklam`;
-- MySQL dump 10.13  Distrib 5.6.13, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: reklam
-- ------------------------------------------------------
-- Server version	5.6.15-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL COMMENT 'Admin''s id (user)',
  `company_id` int(11) NOT NULL COMMENT 'Admin''s owner company',
  `is_superadmin` int(11) NOT NULL COMMENT 'Super admin privilieges',
  KEY `FK_ID_FROM_USER_TO_ADMIN_ID_idx` (`id`),
  KEY `FK_ID_FROM_COMPANY_TO_ADMIN_COMPANY_idx` (`company_id`),
  CONSTRAINT `FK_FROM_COMPANY_ID_TO_ADMIN_COMPANY` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_FROM_USER_ID_TO_ADMIN_ID` FOREIGN KEY (`id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Store admins'' details';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clothes_category`
--

DROP TABLE IF EXISTS `clothes_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clothes_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID of category',
  `name` varchar(45) NOT NULL COMMENT 'Name of the category',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='Categories of clothes';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clothes_category`
--

LOCK TABLES `clothes_category` WRITE;
/*!40000 ALTER TABLE `clothes_category` DISABLE KEYS */;
INSERT INTO `clothes_category` VALUES (5,'Şalvar'),(6,'Köynək'),(7,'Ayaqqabı');
/*!40000 ALTER TABLE `clothes_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clothes_products`
--

DROP TABLE IF EXISTS `clothes_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clothes_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Clothes'' id',
  `store_id` int(11) NOT NULL COMMENT 'Owner store of the product',
  `brand_name` varchar(100) DEFAULT NULL COMMENT 'Manufacturer of the product',
  `season` varchar(20) DEFAULT NULL COMMENT 'Season of the product',
  `category` int(11) DEFAULT NULL COMMENT 'Temprory category',
  `price` decimal(10,0) NOT NULL COMMENT 'Price of the product',
  `image` varchar(250) DEFAULT NULL COMMENT 'Location of the image file',
  `modified_date` datetime NOT NULL COMMENT 'Date when information last modified',
  `discount` decimal(10,0) DEFAULT NULL COMMENT 'Discount on product',
  `description` varchar(500) DEFAULT NULL COMMENT 'Description of the product',
  PRIMARY KEY (`id`),
  KEY `FK_ID_FROM_STORE_TO_CLOTHES_STORE_idx` (`store_id`),
  KEY `FK_FROM_CLCATEGORY_ID_TO_CLPRODUCTS_CATEGORY_idx` (`category`),
  CONSTRAINT `FK_FROM_CLCATEGORY_TO_CLPRODUCTS_CATEGORY` FOREIGN KEY (`category`) REFERENCES `clothes_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_FROM_STORE_ID_TO_CLOTHES_STORE` FOREIGN KEY (`store_id`) REFERENCES `store` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='Clohes table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clothes_products`
--

LOCK TABLES `clothes_products` WRITE;
/*!40000 ALTER TABLE `clothes_products` DISABLE KEYS */;
INSERT INTO `clothes_products` VALUES (1,1,'LTB','Yay',NULL,26,'/images/ltb_yay.jpg','2014-01-26 21:26:05',NULL,'Le jeans'),(2,1,'LTB','Qış',NULL,30,'/images/ltb_qis.jpg','2014-01-26 21:26:30',NULL,'Le jeans'),(3,1,'LTB','Payız',NULL,20,'/images/ltb_payiz.jpg','2014-01-26 21:26:56',NULL,'Le jeans'),(4,1,'Colins','Payız',NULL,20,'/images/colins_payiz.jpg','2014-01-26 21:26:56',NULL,'Le jeans'),(5,1,'Colins','Payız',NULL,32,'/images/colins_payiz2.jpg','2014-01-26 21:31:56',NULL,'Le jeans'),(6,1,'Colins','Qış',NULL,32,'/images/colins_qis.jpg','2014-01-26 21:31:56',NULL,'Le jeans'),(7,1,'Colins','Yay',NULL,39,'/images/colins_yay.jpg','2014-01-26 21:35:56',NULL,'Le jeans');
/*!40000 ALTER TABLE `clothes_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Company id',
  `name` varchar(300) NOT NULL COMMENT 'Company name',
  `address` varchar(300) NOT NULL COMMENT 'Company address',
  `email` varchar(100) NOT NULL COMMENT 'Company email',
  `website` varchar(100) DEFAULT NULL COMMENT 'Company Website',
  `phone_number` varchar(45) NOT NULL COMMENT 'Company phone number',
  `mobile_number` varchar(45) NOT NULL COMMENT 'Company mobile number',
  `facebook_page` varchar(100) DEFAULT NULL COMMENT 'Company''s facebook page',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Company details';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company`
--

LOCK TABLES `company` WRITE;
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
INSERT INTO `company` VALUES (1,'Codex','Xirdalan','codex@gmail.com','codex.az','0186487985','0515671264',NULL),(2,'AzerNet','Xirdalan','azernet@gmail.com','azer.net','0186485748','0515671216',NULL);
/*!40000 ALTER TABLE `company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `restaurant_category`
--

DROP TABLE IF EXISTS `restaurant_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restaurant_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id of categories',
  `name` varchar(45) NOT NULL COMMENT 'Name of the categories',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Categories for restaurant products';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restaurant_category`
--

LOCK TABLES `restaurant_category` WRITE;
/*!40000 ALTER TABLE `restaurant_category` DISABLE KEYS */;
INSERT INTO `restaurant_category` VALUES (1,'Yemək'),(2,'İçki'),(3,'Desert');
/*!40000 ALTER TABLE `restaurant_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `restaurant_products`
--

DROP TABLE IF EXISTS `restaurant_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restaurant_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Product (restaurant) id',
  `store_id` int(11) NOT NULL COMMENT 'Owner store of product',
  `name` varchar(100) NOT NULL COMMENT 'Product''s name',
  `price` decimal(10,0) NOT NULL COMMENT 'Product''s price',
  `image` varchar(250) DEFAULT NULL COMMENT 'Image location',
  `discount` decimal(10,0) DEFAULT NULL COMMENT 'Discount on price',
  `description` varchar(500) DEFAULT NULL COMMENT 'Product''s description',
  `modified_date` datetime NOT NULL COMMENT 'Date when information renewed',
  `cusine` varchar(100) DEFAULT NULL COMMENT 'Cusine of meals',
  `category` int(11) DEFAULT NULL COMMENT 'Temprorary category',
  PRIMARY KEY (`id`),
  KEY `FK_ID_FROM_STORE_TO_REATAURANT_ID_idx` (`store_id`),
  KEY `FK_FROM_RESCATEGORY_TO_RESPRODUCTS_CATEORY_idx` (`category`),
  CONSTRAINT `FK_FROM_RESCATEGORY_TO_RESPRODUCTS_CATEORY` FOREIGN KEY (`category`) REFERENCES `restaurant_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_FROM_STORE_ID_TO_RESTAURANT_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='Products of restaurants';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restaurant_products`
--

LOCK TABLES `restaurant_products` WRITE;
/*!40000 ALTER TABLE `restaurant_products` DISABLE KEYS */;
INSERT INTO `restaurant_products` VALUES (6,2,'Cola',5,'/images/cola.jpg',NULL,'Le Cola','2014-01-26 21:40:56','Qərb',2),(7,2,'Kartof',5,'/images/kartof_fri.jpg',NULL,'Le Kartof','2014-01-26 21:45:56','Qərb',1),(8,2,'Dondurma',9,'/images/dondurma.jpg',NULL,'Le Marojna','2014-01-26 21:47:56','Türk',3),(9,2,'Dolma',5,'/images/dolma.jpg',NULL,'Le Dolma','2014-01-26 21:48:56','Azəri',1),(10,2,'Ayran',1,'/images/ayran.jpg',NULL,'Le Ayran','2014-01-26 21:49:56','Türk',2),(11,2,'Muhallebi',4,'/images/muhallebi.jpg',NULL,'Le Muhallebi','2014-01-26 21:50:24','Türk',3);
/*!40000 ALTER TABLE `restaurant_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `review`
--

DROP TABLE IF EXISTS `review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `review` (
  `id` int(11) NOT NULL COMMENT 'Review''s id',
  `reviewer_id` int(11) NOT NULL COMMENT 'Reviewer''s id (user)',
  `review_date` datetime NOT NULL COMMENT 'Review'' date',
  `review_content` varchar(500) DEFAULT NULL COMMENT 'Review''s description',
  `review_stars` int(11) NOT NULL COMMENT 'Review''s stars',
  `reviewed_item_id` int(11) NOT NULL COMMENT 'Reviewed item''s id',
  PRIMARY KEY (`id`),
  KEY `FK_ID_FROM_USER_idx` (`reviewer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Review details';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `review`
--

LOCK TABLES `review` WRITE;
/*!40000 ALTER TABLE `review` DISABLE KEYS */;
/*!40000 ALTER TABLE `review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `store`
--

DROP TABLE IF EXISTS `store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `store` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Store''s id',
  `name` varchar(100) NOT NULL COMMENT 'Store''s name',
  `company_id` int(11) NOT NULL COMMENT 'Owner company of store',
  `address` varchar(100) NOT NULL COMMENT 'Store''s address',
  `phone_number` varchar(45) DEFAULT NULL COMMENT 'Store''s phone number',
  PRIMARY KEY (`id`),
  KEY `FK_ID_FROM_COMPANY_idx` (`company_id`),
  CONSTRAINT `FK_FROM_COMPANY_ID_TO_STORE_COMPANY` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Store details';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `store`
--

LOCK TABLES `store` WRITE;
/*!40000 ALTER TABLE `store` DISABLE KEYS */;
INSERT INTO `store` VALUES (1,'Codex1',1,'Sumqayit','0186485748'),(2,'Nano',2,'Sumqayit','0186489856');
/*!40000 ALTER TABLE `store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'User''s id',
  `name` varchar(100) NOT NULL COMMENT 'User''s name',
  `surname` varchar(100) NOT NULL COMMENT 'User''s surname',
  `password` varchar(300) NOT NULL COMMENT 'User''s password',
  `email` varchar(100) NOT NULL COMMENT 'User''s email',
  `address` varchar(45) DEFAULT NULL COMMENT 'User''s address',
  `mobile_number` varchar(45) DEFAULT NULL COMMENT 'User''s mobile number',
  `facebook_profile` varchar(100) DEFAULT NULL COMMENT 'User''s facebook url',
  `home_coordinates` varchar(45) DEFAULT NULL COMMENT 'User''s home coordinates',
  `birthday` varchar(45) DEFAULT NULL COMMENT 'User''s birtday',
  `gender` varchar(45) DEFAULT NULL COMMENT 'User''s gender',
  `status` int(11) NOT NULL COMMENT 'Status of the user (Active or Deactive)',
  `registration_date` datetime NOT NULL COMMENT 'Registration date of user',
  `confirm_string` varchar(500) NOT NULL COMMENT 'Confirmation string',
  `last_login_date` datetime DEFAULT NULL COMMENT 'Last login date of the user',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='User Details';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-01-27 21:28:27
